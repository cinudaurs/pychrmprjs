__author__ = 'sthangalapally'



__author__ = 'srini'

# globals
visited = {}
finish = {}
leader = {}

N = 875714
nodes_processed = 0  # current no of nodes processed
source_vert = 0  # current source vertex


def load_graph(filename):
    """
    Function that loads a graph given the URL
    for a text representation of the graph

    Returns a dictionary that models a graph
    """
    graph_file = f = open(filename, 'r')
    graph_text = graph_file.read()
    graph_lines = graph_text.split('\n')
    graph_lines = graph_lines[: -1]

    print "Loaded graph with", len(graph_lines), "nodes"

    G = {}
    RevG = {}

    for line in graph_lines:
        v1 = int(line.split()[0])
        v2 = int(line.split()[1])

        G[v1] = []
        RevG[v2] = []

        G[v1].append(v2)
        RevG[v2].append(v1)

    f.close()

    return G, RevG


def init():
    for i in range(1, N + 1):
        visited[i] = 0
        finish[i] = 0
        leader[i] = 0


def dfs(G, source_node):

    global nodes_processed
    global source_vert

    visited[source_node] = 1
    leader[source_node] = source_vert

    for node in G[source_node]:
        if visited[node] == 0:
            dfs(G, node)

    nodes_processed += 1

    finish[source_node] = nodes_processed


def dfs_loop(G):
    global nodes_processed
    global source_vert

    current_label = N  # current_label : to keep track of ordering

    while current_label > 0:

        if visited[current_label] == 0:
            source_vert = current_label
            dfs(G, current_label)

        finish[source_vert] = current_label

        current_label -= 1


if __name__ == "__main__":

    init()

    G, RevG = load_graph("/home/srini/pycharmProjects/test/SCC.txt")

    dfs_loop(RevG)  # results in finish populated with ordering of nodes

    # build a new graph with ordering of nodes as in finish
    newG = {}

    for i in range(1, N + 1):
        temp = []
        for x in G[i]:
            temp.append(finish[x])

        newG[finish[i]] = temp

    init()

    dfs_loop(newG)
