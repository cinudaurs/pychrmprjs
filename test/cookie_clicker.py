import math

__author__ = 'Srinivas'
"""
Cookie Clicker Simulator
"""

import simpleplot

# Used to increase the timeout, if necessary
import codeskulptor
codeskulptor.set_timeout(20)

import poc_clicker_provided as provided

# Constants
SIM_TIME = 112.0

class ClickerState:
    """
    Simple class to keep track of the game state.
    """

    def __init__(self):
        self.total_cookies_produced = 0.0
        self.current_time = 0.0
        self.cookies_gen_rate_per_sec = 1.0
        self.current_cookies = 0.0
        """
        [current_time, item_bought, cost, tota_cookies_produced]
        """
        self.game_history_lst = [(0.0,None,0.0,0.0)]


    def __str__(self):
        """
        Return human readable state
        """
        printStr = "Total cookies produced "+str(self.total_cookies_produced)+'\n'\
                   +"Current time "+str(self.current_time)+'\n'\
                   +"Cookies per sec "+str(self.cookies_gen_rate_per_sec)+'\n'\
                   +"Current num cookies "+str(self.current_cookies)+'\n'\

        return printStr

    def get_cookies(self):
        """
        Return current number of cookies
        (not total number of cookies)

        Should return a float
        """
        return self.current_cookies

    def get_cps(self):
        """
        Get current CPS

        Should return a float
        """
        return self.cookies_gen_rate_per_sec

    def get_time(self):
        """
        Get current time

        Should return a float
        """
        return self.current_time

    def get_history(self):
        """
        Return history list

        History list should be a list of tuples of the form:
        (time, item, cost of item, total cookies)

        For example: (0.0, None, 0.0, 0.0)
        """
        return self.game_history_lst

    def time_until(self, cookies):
        """
        Return time until you have the given number of cookies
        (could be 0 if you already have enough cookies)

        Should return a float with no fractional part
        """
        if cookies < 0:
            return 0.0

        if cookies <= self.current_cookies:
            return 0.0

        else:

            num_cookies_needed = cookies - self.get_cookies()

            time_until = num_cookies_needed/self.get_cps()

            return math.ceil(time_until)

    def wait(self, time):
        """
        Wait for given amount of time and update state

        Should do nothing if time <= 0
        """
        if time <= 0:
            return None
        else:
            self.current_time += time
            self.current_cookies += time * self.cookies_gen_rate_per_sec
            self.total_cookies_produced += (time * self.cookies_gen_rate_per_sec)


    def buy_item(self, item_name, cost, additional_cps):
        """
        Buy an item and update state

        Should do nothing if you cannot afford the item
        """

        if self.current_cookies >= cost:
            self.current_cookies -= cost
            self.cookies_gen_rate_per_sec += additional_cps
            self.game_history_lst.append((self.current_time, item_name, cost , self.total_cookies_produced))
        else:
            return None


def simulate_clicker(build_info, duration, strategy):
    """
    Function to run a Cookie Clicker game for the given
    duration with the given strategy.  Returns a ClickerState
    object corresponding to game.
    """
    clkr_state_object = ClickerState()
    my_build_info = build_info.clone()

    while duration >= clkr_state_object.get_time() :

         if duration - clkr_state_object.get_time() < 0:
             break

         item_name = strategy(clkr_state_object.get_cookies(),clkr_state_object.get_cps(), (duration - clkr_state_object.get_time()),my_build_info)

         if item_name == None :
             break

         time_until = clkr_state_object.time_until(my_build_info.get_cost(item_name))

         if duration - (clkr_state_object.get_time()+ time_until) < 0:
             break



         clkr_state_object.wait(time_until)
         clkr_state_object.buy_item(item_name, my_build_info.get_cost(item_name), my_build_info.get_cps(item_name))
         my_build_info.update_item(item_name)

    clkr_state_object.wait(duration - clkr_state_object.get_time())

    item_name = strategy(clkr_state_object.get_cookies(),clkr_state_object.get_cps(), duration - clkr_state_object.get_time(),my_build_info)

    if item_name == None :
          return clkr_state_object
    else:
         clkr_state_object.buy_item(item_name, my_build_info.get_cost(item_name), my_build_info.get_cps(item_name))
         my_build_info.update_item(item_name)

    return clkr_state_object


def strategy_cursor(cookies, cps, time_left, build_info):
    """
    Always pick Cursor!

    Note that this simplistic strategy does not properly check whether
    it can actually buy a Cursor in the time left.  Your strategy
    functions must do this and return None rather than an item you
    can't buy in the time left.
    """
    return "Cursor"

def strategy_none(cookies, cps, time_left, build_info):
    """
    Always return None

    This is a pointless strategy that you can use to help debug
    your simulate_clicker function.
    """
    return None

def strategy_cheap(cookies, cps, time_left, build_info):


    cheap_cost = float('inf')
    cheap_item = None
    item_list = build_info.build_items()

    for dummy_i in item_list:
        if build_info.get_cost(dummy_i) < cheap_cost:
            cheap_item_value = build_info.get_cost(dummy_i)
            cheap_item = dummy_i

        print cheap_item
        if build_info.get_cost(cheap_item) <= ((time_left * cps) + cookies):
            return None
        else:
            return cheap_item


def strategy_expensive(cookies, cps, time_left, build_info):


    highest = float('-Inf')
    most_expensive = ""
    item_list = build_info.build_items()
    for dummy_i in item_list:
        if cookies >= build_info.get_cost(dummy_i) and build_info.get_cost(dummy_i) > highest:
            highest = build_info.get_cost(dummy_i)
            most_expensive = dummy_i
    if highest > cookies + cps * time_left:
        return None
    else:
        return most_expensive

def strategy_best(cookies, cps, time_left, build_info):
    return None

def run_strategy(strategy_name, time, strategy):
    """
    Run a simulation with one strategy
    """
    state = simulate_clicker(provided.BuildInfo(), time, strategy)
    print strategy_name, ":", state

    # Plot total cookies over time

    # Uncomment out the lines below to see a plot of total cookies vs. time
    # Be sure to allow popups, if you do want to see it

    # history = state.get_history()
    # history = [(item[0], item[3]) for item in history]
    # simpleplot.plot_lines(strategy_name, 1000, 400, 'Time', 'Total Cookies', [history], True)

def run():
    """
    Run the simulator.
    """
    run_strategy("Cursor", SIM_TIME, strategy_cursor)

    # Add calls to run_strategy to run additional strategies
    # run_strategy("Cheap", SIM_TIME, strategy_cheap)
    # run_strategy("Expensive", SIM_TIME, strategy_expensive)
    # run_strategy("Best", SIM_TIME, strategy_best)

#run()

#import user34_muNP84fR8e_2 as testsuite
#testsuite.run_tests(ClickerState,simulate_clicker,strategy_cursor)