__author__ = 'Srinivas'
import sys
from bin_heap import BHeap


# sample graph with edge weights
#graph = { 0: {1:6, 3:18, 2:8},
#          1: {4:11},
#          2: {3:9},
#          3: {},
#          4: {5:3},
#          5: {3:4, 2:7}
#    }

graph = { 0: [(1,6), (3,18), (2,8)],
          1: [(4,11)],
          2: [(3,9)],
          3: [],
          4: [(5,3)],
          5: [(3,4), (2,7)]
    }



# pred = {0:0, 1:0, 2:0, 3:17, 4:17, 5:20} - check for this case to find the bug.

def load_graph(filename):
    """
    Function that loads a graph given the URL
    for a text representation of the graph

    Returns a dictionary that models a graph
    """
    graph_file = f = open(filename, 'r')
    lines = graph_file.read().splitlines()

    G = {}

    for line in lines:

        data = line.split()
        v1 = int(data[0])
        G[v1] = []

        tmp_list = []

        for tpl in data[1:]:
            adj_v, wt = tpl.split(',')
            adj_v = int(adj_v)
            wt = int(wt)
            tmp_list.append((adj_v, wt))

        G[v1] = tmp_list

    return G





def singleSourceShortestPath(graph, s):
    """Compute and return (dist, pred) matrices of computation"""

    pq = BHeap(len(graph))
    dist = {}
    pred = {}

    for v in graph:
        dist[v] = sys.maxint
        pred[v] = None
    dist[s] = 0

    for v in graph:
        pq.insert(v, dist[v])

    while not pq.isEmpty():
        u = pq.smallest()

        for v in graph[u]:
            wt = v[1]
            newLen = dist[u] + wt

            if newLen < dist[v[0]]:
                pq.decreaseKey(v[0], newLen)
                dist[v[0]] = newLen
                pred[v[0]] = u

    return (dist, pred)


def shortest_path(s, v, dist, pred):
    """Return path and total information for shortest path s to v """
    path = [v]
    total = dist[v]
    while v != s:
        v = pred[v]
        path.insert(0, v)

    return "length=" + str(total) + " path=  " + str(path)


if __name__ == "__main__":

    G = load_graph("dijkstraData.txt")

    #print G

    #G = load_graph("dijkstra_simple.txt")

    (dist, pred) = singleSourceShortestPath(G,1)
    print (dist,pred)
    dist_to_vertices = [7,37,59,82,99,115,133,165,188,197]
    #dist_to_vertices = [3]

    for i in dist_to_vertices:
        print shortest_path(1,i, dist, pred)