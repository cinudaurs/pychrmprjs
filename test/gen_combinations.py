__author__ = 'Srinivas'

import itertools
from itertools import product

def gen_combinations(v):

    v = 0b1100100100100111
    t = ((v | (v - 1)) + 1)
    w = bin(t | ((((t & -t) / (v & -v)) >> 1) - 1))




def kbits(s, n, k):
    result = []

    for bits in itertools.combinations(range(n), k):
        temp = []
        for idx, elem in enumerate(s):
            for bit in bits:
                if idx == bit:
                    temp+=elem
        result.append(''.join(temp))
    return result


def hash (s):
    h = 7
    letters = "acdegilmnoprstuw"
    for i in range(len(s)):
        h = (h * 37 + letters.index(s[i]))
    return h


def check_hash():
    temp =[]
    for roll in product(['a','c','d','e','g','i','l','m','n','o','p','r','s','t','u','w'], repeat = 8):
        temp = ''.join(roll)
        hash_str =  str(temp)
        if hash(hash_str) == 25180466553932 :
            print hash_str
            break

        #if hash(str(temp)) == 680131659347 :
        #    print temp
        #    break


#def check_hash():
#    s = ['a','c','d','e','g','i','l','m','n','o','p','r','s','t','u','w']
#    result = kbits(s, 16, 8)
#    temp = []
#    for res in result:
#        perms = itertools.permutations(res)
#        for perm in perms:
#            temp.append(''.join(perm))
#            if temp == 'leepadg':
#                print temp
#            #temp_hash = hash(temp)
#            #if temp_hash == 25180466553932:
#                #return temp

#print hash('leepadg')
check_hash()
