import sys
import math
import os
import json

__author__ = 'srini'


def getCustomerRecords(filename):
    """read the customer records from file and yield a json object at a time"""

    cust_file = open(filename, 'rb')
    data = cust_file.readlines()

    for line in data :
        values = json.loads(line)
        yield values


def great_circle_dist(cust_long, cust_lat, target_long, target_lat):

    radius = 6371 #km

    x = math.pi/180.0  #to convert from degree to radians

    a = (90.0 - float(cust_lat))*(x)
    b = (90.0 - float(target_lat))*(x)
    theta = (float(target_long) - float(cust_long))*(x)

    c = math.acos((math.sin(a)*math.sin(b)) + (math.cos(a)*math.cos(b)*math.cos(theta)))

    return radius*c



if __name__ == "__main__":

    #dublin office loc
    target_long = -6.2592576
    target_lat = 53.3381985

    result_list_of_users = []

    cust_records = getCustomerRecords('customers.txt')

    for cust in cust_records:

        distance = great_circle_dist(cust['longitude'], cust['latitude'], target_long, target_lat)

        if distance <= 100:
            result_list_of_users.append((cust['user_id'], str(cust['name'])))


    for item in sorted(result_list_of_users, key=lambda tup: tup[0]):
         print item

