__author__ = 'srini'

"""
 Print elements of a linked list on console
 head input could be None as well for empty list
 Node is defined as

"""

class Node(object):

   def __init__(self, data=None, next_node=None):
       self.data = data
       self.next = next_node


class LinkedList(object):


    def __init__(self):
        self.head = None


    def print_list(self,head):

        while(head is not None):

            print head.data
            head = head.next


    def InsertAtTail(self,head, data):
        if head == None:
            return Node(data, None)
        else:
            if head.next == None:
                head.next = Node(data,None)
            else:
                self.InsertAtTail(head.next,data)
            return head


    def InsertAtHead(self,head, data):

        if head == None:
            return Node(data, None)
        else:
            new_node = Node(data)
            new_node.next = head
            head = new_node

        return head


    def InsertNth(self, head, data, position):

        new_node = Node(data)

        if not head or position == 0:
            head = new_node

        else:
            probe = head

            while position > 1 and probe.next != None :
                probe = probe.next
                position -= 1

            temp = probe.next
            probe.next = new_node
            new_node.next = temp

        return head



    def Delete(self, head, position):

        if position <= 0 or head.next is None:
            removedItem = head.data
            head = head.next
            return head
        else:
            probe = head
            while position > 1 and probe.next.next != None:
                probe = probe.next
                position -= 1

            removedItem = probe.next.data
            probe.next = probe.next.next
            return head


    def ReversePrint(self,head):
        if head == None:
            return
        else:
            self.ReversePrint(head.next)
            print head.data


    def ReverseRecur(self,head):

        if head is None or head.next is None:
            return head

        new_head = self.ReverseRecur(head.next) # reverse all but first
        head.next.next = head              # make original second point to first
        head.next = None                   # make original first point at nothing

        return new_head


    def ReverseNonRecur(self,head):

        prev = None
        current = head

        while current is not None:

            next = current.next
            current.next = prev
            prev = current
            current = next

        head = prev

        return head

    def CompareLists(headA, headB):

        while( headA != None and headB !=None and headA.data == headB.data):
            headA = headA.next
            headB = headB.next

        if headA is None or headB is None:
            if headA == headB:
                return 1
            else:
                return 0

        else:
            if (headA.data == headB.data):
                return 1
            else:
                return 0


    def MergeLists(headA, headB):

        if headA is None:
            return headB
        if headB is None:
            return headA


        if headA.data < headB.data:                           #finding the head of resulting list
            mergedList = headA
            headA = headA.next
        else:
            mergedList = headB
            headB = headB.next

        mergeHead = mergedList

        while headA is not None or headB is not None:

            if headA is None:                                  #end of list A (consider just B)
                mergedList.next = headB
                headB = headB.next

            elif headB is None:                                  #end of list B (consider just A)
                mergedList.next = headA
                headA = headA.next

            else:                                               #both lists aren't at the end. Which element to choose?
                if headA.data < headB.data:                    #if A is smaller
                    mergedList.next = headA                     #A will be chosen.
                    mergedList = mergedList.next
                    headA = headA.next

                else:                                           #if B is smaller...
                    mergedList.next = headB                     # B will be chosen.
                    mergedList = mergedList.next
                    headB = headB.next


        return mergeHead


    def GetNode(head, position):

        probe = head
        current = head

        index = 0

        while index < position and current.next is not None:
            current = current.next
            index = index+1

        while current.next != None and probe.next is not None:
            current = current.next
            probe = probe.next


        return probe.data


    def RemoveDuplicates(head):

        probe = head
        if probe is None or probe.next is None:
            return probe

        while probe.next != None:
            if probe.data != probe.next.data:
                probe = probe.next
            else:
                probe.next = probe.next.next

        return head


    def has_cycle(head):

        visited = []
        probe = head

        while(probe != None):
            if probe.next is None:
                return False
            else:
                if probe.data not in visited:
                    visited.append(probe.data)
                    probe = probe.next
                else:
                    return True


        return False


    def FindMergeNode(headA, headB):

        probeA = headA
        probeB = headB

        while probeA != probeB:

            if probeA.next is None:
                probeA = headB
            else:
                probeA = probeA.next

            if probeB.next is None:
                probeB = headA
            else:
                probeB = probeB.next


        return probeB.data








lnkdlst = LinkedList()

head = lnkdlst.head
head = lnkdlst.InsertNth(head, 3, 0)
head = lnkdlst.InsertNth(head, 5, 1)

head = lnkdlst.InsertNth(head, 4, 2)
head = lnkdlst.InsertNth(head, 2, 3)
head = lnkdlst.InsertNth(head, 10, 1)

lnkdlst.print_list(head)

head = lnkdlst.Delete(head, 4)

print '\n'

lnkdlst.print_list(head)