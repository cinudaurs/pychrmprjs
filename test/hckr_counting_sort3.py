__author__ = 'srini'

'''
In the previous challenge, it was easy to print all the integers in order, since you did not have to access the original list.
Once you had obtained the frequencies of all the integers, you could simply print each integer in order the correct number of times.
However, if there is other data associated with an element, you will have to access the original element itself.

In the final counting sort challenge, you are required to print the data associated with each integer.
This means, you will go through the original array to get the data,
and then use some "helper arrays" to determine where to place everything in a sorted array.

If you know the frequencies of each element, you know how many times to place it,
but which index will you start placing it from?
It will be helpful to create a helper array for the "starting values" of each element.

Challenge
You will be given a list that contains both integers and strings. In this challenge you just care about the integers.
For every value  from , can you output , the number of elements that are less than or equal to ?

Input Format
- , the size of the list .
-  lines follow, each containing an integer  and a string .

Output Format
Output  for all numbers from 0 to 99 (inclusive).

sample input:
10
4 that
3 be
0 to
1 be
5 question
1 or
2 not
4 is
2 to
4 the

sample output:

D:
[1, 3, 5, 6, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
-----------------------
c : [1, 2, 2, 1, 3, 1, 0, 0, 0, 0]
-----------------------
b : [[(2, 'to')], [(3, 'be'), (5, 'or')], [(6, 'not'), (8, 'to')], [(1, 'be')], [(0, 'that'), (7, 'is'), (9, 'the')], [(4, 'question')], [], [], [], []]


'''


def counting_sort3(n):

    for i in xrange(n):
        a = raw_input().split()
        num = int(a[0])
        c[num] = c[num] + 1
        b[num].append((i, a[1]))

    D = [sum(c[:i+1]) for i in range(100)]

    print D
    print '-----------------------'
    print c
    print '-----------------------'
    print b











n = input()
c = [0] * n # count
b = [[] for i in xrange(n)] #helper arrray

counting_sort3(n)

