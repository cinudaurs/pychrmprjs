__author__ = 'srini'


class Range(object):

    def __init__(self):

        self.lyst = []


    def add(self, start_val, end_val):

        range_dict = {'start_val': start_val, 'end_val': end_val}
        self.lyst.append(range_dict)

#( [
#    { start_val: 10, end_val: 20 },
#    { start_val: 5, end_val: 15 },
#    { start_val: 16, end_val: 19 },
#    { start_val: 25, end_val: 30 },
#    ], 5, 30)


# Given three parameters, array of Ranges r and two integers start and end, write a method isSaturated(Range[] r, int start, int end) that returns a boolean as to whether or not the collection of Ranges fully covers or "saturates" the range given by the method params start and end.

    def isSaturated(self, lyst, start_val, end_val):

        bool = {}

        for val in xrange(start_val, end_val+1):
             bool[val] = False

        for value in xrange(start_val, end_val+1):

            for item in lyst:

                if value not in xrange(item['start_val'], item['end_val']+1):
                   continue

                else:
                    bool[value] = True


        for key in bool.keys():
            if bool[key] == False:
                return False

        return True




rnge = Range()

rnge.add(10, 20)
rnge.add(5, 15)
rnge.add(16,19)
#rnge.add(25, 30)

lyst = rnge.lyst

print rnge.isSaturated(lyst, 5, 30)

# start_val = 5
# end_val = 10
# #
# #
#
# bool = {}
# truth_table = []
#
#
# print bool

# truth_table = [True for bool[val] in xrange(start_val, end_val+1)]
#
# for idx, val in enumerate(truth_table):
#     print idx, val

# # truth_table = [True for i in xrange((end_val - start_val)+1)]
# #
# def isFalsy():
#     for idx, val in enumerate(truth_table):
#         if truth_table[idx] == True:
#             return False
# #
# print isFalsy()