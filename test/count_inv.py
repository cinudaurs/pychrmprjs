__author__ = 'srini'

# Functions to perform merge sort


def merge_and_countsplit_inv(list1, list2):
    """
    Merge two sorted lists.

    Returns a new sorted list containing all of the elements that
    are in both list1 and list2.

    This function can be iterative.
    """
    split=0
    new_list = []

    while list1 and list2:
        if list1[0] == list2[0]:
            new_list.append(list1.pop(0))
            list2.pop(0)
        elif list1[0] < list2[0]:
            new_list.append(list1.pop(0))
        elif list1[0] > list2[0]:
            new_list.append(list2.pop(0))
            split+=len(list1)

    if list1:
        new_list.extend(list1)
    if list2:
        new_list.extend(list2)

    return (new_list, split)


def merge_sort_and_count(list1):
    """
    Sort the elements of list1.

    Return a new sorted list with the same elements as list1.

    This function should be recursive.
    """

    middle = len(list1) / 2

    if len(list1) <= 1:
        return (list1,0)

    else:
        left_list = list1[:middle]
        right_list = list1[middle:]

        left= merge_sort_and_count(left_list)
        left_list = left[0]

        right = merge_sort_and_count(right_list)
        right_list = right[0]

        split= merge_and_countsplit_inv(left_list,right_list)
        split_list = split[0]

    return (split_list, left[1] + right[1] + split[1])








#print sort_and_count([1,3,5,2,4,6])
#print sort_and_count([1,5,3,2,4])
#print sort_and_count([5,4,3,2,1])
#print sort_and_count([10, 9, 8, 7, 6, 5, 4, 3, 2, 1])
#print sort_and_count([9, 12, 3, 1, 6, 8, 2, 5, 14, 13, 11, 7, 10, 4, 0])
#print sort_and_count([37, 7, 2, 14, 35, 47, 10, 24, 44, 17, 34, 11, 16, 48, 1, 39, 6, 33, 43, 26, 40, 4, 28, 5, 38, 41, 42, 12, 13, 21, 29, 18, 3, 19, 0, 32, 46, 27, 31, 25, 15, 36, 20, 8, 9, 49, 22, 23, 30, 45])

#print merge_sort([25, 73, 68, 72, 61, 76, 60, 18, 31, 80, 36, 50, 86, 20, 51, 78, 23, 95, 66, 94, 17, 11, 89, 10, 4, 57, 44, 59, 58, 99, 27, 56, 38, 26, 67, 65, 84, 46, 83, 47, 64, 98, 41, 74, 85, 37, 13, 93, 14, 53, 48, 71, 49, 92, 87, 82, 69, 3, 22, 28, 8, 97, 32, 81, 34, 29, 62, 16, 24, 33, 75, 40, 19, 79, 6, 45, 35, 42, 5, 77, 39, 1, 9, 15, 12, 52, 54, 30, 91, 2, 21, 70, 7, 63, 55, 90, 88, 0, 43, 96])

result = merge_sort_and_count([16, 216, 179, 202, 2, 86, 107, 76, 8, 203, 114, 41, 207, 125, 44, 84, 138, 32, 245, 77, 168, 112, 35, 62, 66, 143, 26, 246, 175, 214, 237, 181, 171, 40, 121, 19, 109, 240, 58, 150, 101, 82, 230, 31, 80, 235, 153, 56, 12, 123, 146, 188, 89, 59, 53, 217, 195, 21, 110, 74, 113, 47, 210, 17, 206, 165, 229, 54, 226, 144, 100, 205, 60, 45, 184, 211, 154, 198, 104, 180, 213, 24, 173, 185, 38, 103, 105, 48, 142, 223, 145, 148, 78, 93, 108, 81, 130, 218, 194, 57, 1, 201, 135, 95, 127, 233, 43, 88, 9, 155, 141, 187, 157, 51, 22, 115, 182, 225, 69, 122, 176, 163, 177, 50, 28, 248, 34, 249, 49, 75, 97, 193, 7, 68, 149, 189, 242, 151, 170, 111, 160, 71, 167, 243, 178, 236, 46, 70, 120, 14, 172, 39, 30, 199, 162, 36, 140, 6, 42, 96, 67, 116, 119, 219, 159, 200, 222, 72, 204, 197, 174, 37, 152, 27, 164, 129, 99, 239, 221, 156, 5, 137, 102, 18, 241, 227, 166, 3, 238, 106, 98, 191, 196, 224, 13, 134, 79, 11, 33, 23, 131, 15, 10, 29, 244, 147, 20, 186, 73, 0, 128, 232, 132, 192, 139, 126, 247, 61, 208, 94, 183, 136, 190, 52, 55, 169, 133, 158, 25, 87, 4, 65, 64, 90, 91, 161, 92, 63, 209, 118, 83, 234, 231, 220, 117, 124, 228, 212, 215, 85])
print result[1]