__author__ = 'sthangalapally'

"""
Function to generate permutations of outcomes
Repetition of outcomes not allowed
"""

from rcviz import callgraph, viz

@viz
def gen_permutations(outcomes):

    result = []

    if outcomes == "":
         return [""]
    else:
         first = outcomes[0]
         rest =  outcomes[1:]
         rest_strings = list(gen_permutations(rest))
         for rest in rest_strings:
             result += [rest[:x] + first + rest[x:] for x in range(len(rest)+1)]

    return result



    # example for digits
outcomes = "abcd"

gen_permutations(outcomes)

callgraph.render("gen_permutations.png")