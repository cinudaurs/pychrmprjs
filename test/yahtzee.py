"""
Planner for Yahtzee
Simplifications:  only allow discard and roll, only score against upper level
"""

# Used to increase the timeout, if necessary
import codeskulptor
codeskulptor.set_timeout(20)

def gen_all_sequences(outcomes, length):
    """
    Iterative function that enumerates the set of all sequences of
    outcomes of given length.
    """

    answer_set = set([()])
    for dummy_idx in range(length):
        temp_set = set()
        for partial_sequence in answer_set:
            for item in outcomes:
                new_sequence = list(partial_sequence)
                new_sequence.append(item)
                temp_set.add(tuple(new_sequence))
        answer_set = temp_set
    return answer_set


def score(hand):
    """
    Compute the maximal score for a Yahtzee hand according to the
    upper section of the Yahtzee score card.

    hand: full yahtzee hand

    Returns an integer score
    """

    if len(hand)>0:
        result = {}
        for value in hand:
            if value not in result.keys():
                result[value] = value
            else:
                result[value] = result[value] + value
        sum_of_values = max(result.values())
    return sum_of_values


def expected_value(held_dice, num_die_sides, num_free_dice):
    """
    Compute the expected value of the held_dice given that there
    are num_free_dice to be rolled, each with num_die_sides.

    held_dice: dice that you will hold
    num_die_sides: number of sides on each die
    num_free_dice: number of dice to be rolled

    Returns a floating point expected value
    """


    possible_outcomes = []

    sum_of_values = 0

    for index in range(1,num_die_sides+1):
        possible_outcomes.append(index)

    sequence_set = set(gen_all_sequences(possible_outcomes,num_free_dice))

    held_in_hand = held_dice

    for seq in sequence_set:
        held_in_hand = held_in_hand + seq
        sum_of_values = sum_of_values + score(held_in_hand)
        held_in_hand = held_dice

    exp_value = float(sum_of_values)/len(sequence_set)

    return exp_value


def gen_all_holds(hand):
    """
    Generate all possible choices of dice from hand to hold.

    hand: full yahtzee hand

    Returns a set of tuples, where each tuple is dice to hold

    notes:
    https://class.coursera.org/principlescomputing-001/forum/thread?thread_id=2170
    https://class.coursera.org/principlescomputing-001/forum/thread?thread_id=2142

    """

    dice_in_hand = list(hand)

    masks = [list(elem) for elem in gen_all_sequences((0,1),len(hand))]

    possible_hand_to_hold_choices = []

    for dummy_idx in range(0,len(masks)):
        temp = []

        for dummy_jdx in range(0,len(hand)):

            if(masks[dummy_idx][dummy_jdx]!=0):

                temp.append(dice_in_hand[dummy_jdx]* masks[dummy_idx][dummy_jdx])

        possible_hand_to_hold_choices.append(temp)

    result = set([tuple(elem) for elem in possible_hand_to_hold_choices])

    return result


def strategy(hand, num_die_sides):
    """
    Compute the hold that maximizes the expected value when the
    discarded dice are rolled.

    hand: full yahtzee hand
    num_die_sides: number of sides on each die

    Returns a tuple where the first element is the expected score and
    the second element is a tuple of the dice to hold
    """

    hold_list = gen_all_holds(hand)
    max_expected_value = 0.0
    hold = ()
    for held_seq in hold_list:
        exp_value = expected_value(held_seq,num_die_sides,len(hand)-len(held_seq))
        if max_expected_value < exp_value:
            max_expected_value = exp_value
            hold = held_seq

    return (max_expected_value, hold)


def run_example():
    """
    Compute the dice to hold and expected score for an example hand
    """
    num_die_sides = 6
    hand = (1, 1, 1, 5, 6)
    hand_score, hold = strategy(hand, num_die_sides)
    print "Best strategy for hand", hand, "is to hold", hold, "with expected score", hand_score


    #run_example()