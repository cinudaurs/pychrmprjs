__author__ = 'Srinivas'

#a_list = [3,2,8,5,1,4,7,6]
compare_count = 0
first = 0
final = 1
median = 2

def qksort(a_list, n, pivot_type):

    qk_sort(a_list, 0, len(a_list)-1, pivot_type)


def swap(a_list, i, j):
    temp = a_list[j]
    a_list[j] = a_list[i]
    a_list[i] = temp

def is_median(ar, i, j, k):
    '''
    Determines whether ar[i] is a median
    of ar[i], ar[j] and ar[k].
    '''
    return (ar[i] < ar[j] and ar[i] > ar[k]) or\
           (ar[i] > ar[j] and ar[i] < ar[k])


def qk_sort(a_list, l, r, pivot_type):

    global compare_count

    if l >= r:
        return

    if pivot_type == first:
        pivot = a_list[l]

    elif pivot_type == final:
        pivot = a_list[r]
        swap(a_list, l, r)

    elif pivot_type == median:
        m = l + ((r-l) >> 1)
        if is_median(a_list, l, m, r):
            pivot = a_list[l]
        elif is_median(a_list, m, l, r):
            pivot = a_list[m]
            swap(a_list, l, m)
        else:
            pivot = a_list[r]
            swap(a_list, l, r)


    compare_count += r-l

    i = l + 1

    for j in range(i,r+1):
        if a_list[j] < pivot:
            #swap A[j] and A[i]
            swap(a_list, i, j)
            i+=1
    #swap A[l] and A[i-1]
    swap(a_list,l, i-1)
        
    qk_sort(a_list, l, i-2, pivot_type)

    qk_sort(a_list, i, r, pivot_type)



f = open('QuickSort.txt', 'r')
provided_list = []

for line in f.readlines():
    provided_list.append(int(line))

compare_count = 0
a_list = provided_list[:]
qksort(a_list, len(a_list), first)
print compare_count

compare_count=0
a_list = provided_list[:]
qksort(a_list, len(a_list), final)
print compare_count

compare_count=0
a_list = provided_list[:]
qksort(a_list, len(a_list), median)
print compare_count




