"""
Analysis of RNA structure and Pairwise
alignments either global or local
"""

import pandas as pd


# def build_scoring_matrix(alphabet, diag_score, off_diag_score, i_dash_score, j_dash_score):
#     """
#     Build scoring matrix
#     Score on pair of alphabets
#     Return as dictionary of dictionaries
#     """
#     alphabets = list(alphabet)
#     alphabets.append('-')
#     scoring_matrix = {}
#     for alpha in alphabets:
#         scoring_matrix[alpha] = {}
#
#     for entry in scoring_matrix:
#         for alpha in alphabets:
#             if entry == alpha and entry != '-':
#                 scoring_matrix[entry][alpha] = diag_score
#
#             elif entry == '-' and alpha == '-':
#                 scoring_matrix[entry][alpha] = '-'
#
#             elif entry == '-':
#                 scoring_matrix[entry][alpha] = i_dash_score
#
#             elif alpha == '-':
#                 scoring_matrix[entry][alpha] = j_dash_score
#
#             else:
#                 scoring_matrix[entry][alpha] = off_diag_score
#
#     return scoring_matrix

def build_scoring_matrix(alphabet, diag_score, off_diag_score, dash_score):
    """
    Build scoring matrix
    Score on pair of alphabets
    Return as dictionary of dictionaries
    """
    alphabets = list(alphabet)
    alphabets.append('-')

    scoring_matrix = {}

    for alphabet in alphabets:
        scoring_matrix[alphabet] = {}

    for entry in scoring_matrix:
        for alphabet in alphabets:
            if entry == alphabet and entry != '-':
                scoring_matrix[entry][alphabet] = diag_score

            elif entry == '-' or alphabet == '-':
                scoring_matrix[entry][alphabet] = dash_score

            else:
                scoring_matrix[entry][alphabet] = off_diag_score

    return scoring_matrix






def compute_alignment_matrix(seq_x, seq_y, scoring_matrix, global_flag):
    """
    Given two sequence, a scoring matrix,
    Computes a DP alignment matrix
    """
    length_of_seq_x = len(seq_x)
    length_of_seq_y = len(seq_y)
    

    
    alignment_matrix = [[] for seq in range(length_of_seq_x + 1)]

    #init the grid with zeros
    for seq_elem_x in range(length_of_seq_x + 1):
        for seq_elem_y in range(length_of_seq_y + 1):
            alignment_matrix[seq_elem_x].append(0)
    

    
    for seq_elem_x in range(1, length_of_seq_x + 1):

        
        alignment_matrix[seq_elem_x][0] = alignment_matrix[seq_elem_x - 1][0] + scoring_matrix[seq_x[seq_elem_x - 1]]['-']
        
        if global_flag == False:
            if alignment_matrix[seq_elem_x][0] < 0:
                alignment_matrix[seq_elem_x][0] = 0
        
    for seq_elem_y in range(1, length_of_seq_y + 1):
        alignment_matrix[0][seq_elem_y] = alignment_matrix[0][seq_elem_y - 1] + scoring_matrix['-'][seq_y[seq_elem_y - 1]]
        
        if global_flag == False:
            if alignment_matrix[0][seq_elem_y] < 0:
                alignment_matrix[0][seq_elem_y] = 0
        
    for seq_elem_x in range(1, length_of_seq_x + 1):
        for seq_elem_y in range(1, length_of_seq_y + 1):

            alignment_matrix[seq_elem_x][seq_elem_y] = max(alignment_matrix[seq_elem_x - 1][seq_elem_y - 1] + scoring_matrix[seq_x[seq_elem_x - 1]][seq_y[seq_elem_y - 1]],
                                               alignment_matrix[seq_elem_x - 1][seq_elem_y] + scoring_matrix[seq_x[seq_elem_x - 1]]['-'],
                                               alignment_matrix[seq_elem_x][seq_elem_y - 1] + scoring_matrix['-'][seq_y[seq_elem_y - 1]])
            
            if global_flag == False:
                if alignment_matrix[seq_elem_x][seq_elem_y] < 0:
                    alignment_matrix[seq_elem_x][seq_elem_y] = 0
            
    return alignment_matrix

def compute_global_alignment(seq_x, seq_y, scoring_matrix, alignment_matrix):
    """
    Given two sequences, a scoring matrix
    and a DP alignment matrix
    Compute global alignment of the form (score, align_x, align_y)
    """
    length_of_seq_x = len(seq_x)
    length_of_seq_y = len(seq_y)
    
    score = alignment_matrix[length_of_seq_x][length_of_seq_y]
    
    align_x = ""
    align_y = ""
    
    while length_of_seq_x != 0 and length_of_seq_y != 0:
        
        if alignment_matrix[length_of_seq_x][length_of_seq_y] == alignment_matrix[length_of_seq_x - 1][length_of_seq_y - 1] + scoring_matrix[seq_x[length_of_seq_x - 1]][seq_y[length_of_seq_y - 1]]:
            
            align_x = seq_x[length_of_seq_x - 1] + align_x
            
            align_y = seq_y[length_of_seq_y - 1] + align_y

            length_of_seq_x -= 1
            length_of_seq_y -= 1
            
        else:
            if alignment_matrix[length_of_seq_x][length_of_seq_y] == alignment_matrix[length_of_seq_x - 1][length_of_seq_y] + scoring_matrix[seq_x[length_of_seq_x - 1]]['-']:
                
                align_x = seq_x[length_of_seq_x - 1] + align_x
                
                align_y = '-' + align_y
                
                length_of_seq_x -= 1

            else:
                
                align_x = '-' + align_x
                
                align_y = seq_y[length_of_seq_y - 1] + align_y
                
                length_of_seq_y -= 1

                
    while length_of_seq_x != 0:
        
        align_x = seq_x[length_of_seq_x - 1] + align_x
        
        align_y = '-' + align_y
        
        length_of_seq_x -= 1
        
    while length_of_seq_y != 0:
        
        align_x = '-' + align_x
        
        align_y = seq_y[length_of_seq_y - 1] + align_y
        
        length_of_seq_y -= 1


    return (score, align_x, align_y)


def compute_local_alignment(seq_x, seq_y, scoring_matrix, alignment_matrix):
    """
    Given two sequences, a scoring matrix
    and a DP alignment matrix
    Compute global alignment of the form (score, align_x, align_y)
    """
    length_of_seq_x = None
    length_of_seq_y = None
    
    score = float("-inf")
    
    for dummy_row in range(len(seq_x) + 1):
        for dummy_col in range(len(seq_y) + 1):
            
            if alignment_matrix[dummy_row][dummy_col] > score:
                
                score = int(alignment_matrix[dummy_row][dummy_col])
                
                length_of_seq_x = dummy_row
                length_of_seq_y = dummy_col
                
    align_x = ""
    align_y = ""
    
    while alignment_matrix[length_of_seq_x][length_of_seq_y] != 0:
        
        if alignment_matrix[length_of_seq_x][length_of_seq_y] == alignment_matrix[length_of_seq_x - 1][length_of_seq_y - 1] + scoring_matrix[seq_x[length_of_seq_x - 1]][seq_y[length_of_seq_y - 1]]:
            
            align_x = seq_x[length_of_seq_x - 1] + align_x
            
            align_y = seq_y[length_of_seq_y - 1] + align_y

            length_of_seq_x -= 1
            length_of_seq_y -= 1
            
        else:
            if alignment_matrix[length_of_seq_x][length_of_seq_y] == alignment_matrix[length_of_seq_x - 1][length_of_seq_y] + scoring_matrix[seq_x[length_of_seq_x - 1]]['-']:

                align_x = seq_x[length_of_seq_x - 1] + align_x
                
                align_y = '-' + align_y
                
                length_of_seq_x -= 1

            else:
                
                align_x = '-' + align_x
                
                align_y = seq_y[length_of_seq_y - 1] + align_y
                
                length_of_seq_y -= 1
    
    return (score, align_x, align_y)


scoring_matrix = build_scoring_matrix(['A', 'C', 'T', 'G'], 6, 2, -4)

a = pd.DataFrame(scoring_matrix)

print a.to_string(na_rep='-')


dp_align = compute_alignment_matrix('AC', 'GG', scoring_matrix, True )



print compute_global_alignment('AC', 'GG', scoring_matrix, dp_align)

a = pd.DataFrame(dp_align)

print a.to_string(na_rep='-')
