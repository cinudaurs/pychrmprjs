from shutil import copy

__author__ = 'srini'

swap_count = 0

def swap(lyst, i, j):


    """Exchanges the items at positions i and j."""
    # You could say lyst[i], lyst[j] = lyst[j], lyst[i]
    #  but the following code shows what is really going on
    temp = lyst[i]
    lyst[i] = lyst[j]
    lyst[j] = temp




def selectionSort(lyst):
    i = 0
    while i < len(lyst) - 1:         # Do n - 1 searches
        minIndex = i                 # for the smallest
        j = i + 1
        while j < len(lyst):         # Start a search
            if lyst[j] < lyst[minIndex]:
                minIndex = j
                j += 1
        if minIndex != i:            # Exchange if needed
            swap(lyst, minIndex, i)
        i += 1


def bubbleSort(lyst):
    n = len(lyst)
    while n > 1:                  # Do n - 1 bubbles
        swapped = False
        i = 1                     # Start each bubble
        while i < n:
            if lyst[i] < lyst[i - 1]: # Exchange if needed
                swap(lyst, i, i - 1)
                swapped = True
            i += 1

        if not swapped: return        # Return if no swaps.
        n -= 1



def insertionSort(lyst):

    count = 0

    i = 1
    while i < len(lyst):

        itemToInsert = lyst[i]
        j = i-1

        while j >= 0:

            if itemToInsert < lyst[j]:
                lyst[j+1] = lyst[j]
                count+=1

                j = j-1

            else:
                break

        lyst[j+1] = itemToInsert


        i+=1

    return count


def quicksort(lyst):

    quicksortHelper(lyst, 0, len(lyst)-1)




def quicksortHelper(lyst, left, right):

    if left < right:
        pivotLocation = partition(lyst, left, right)
        quicksortHelper(lyst, left, pivotLocation-1)
        quicksortHelper(lyst, pivotLocation+1, right)



def partition(lyst, left, right):

    global swap_count

    pivot = lyst[right]

    # Set boundary point to first position
    boundary = left

    # Move items less than pivot to the left
    for index in range(left, right):
        if lyst[index] < pivot:
           swap(lyst, index, boundary)
           swap_count+=1
           boundary += 1


    # Exchange the pivot item and the boundary item
    swap(lyst, right, boundary)
    swap_count+=1

    return boundary


def main():
    lyst = []

    n = int(raw_input().strip())
    lyst = map(int,raw_input().strip().split(' '))

    arr = list(lyst)

    quicksort(arr)
    print swap_count

    arr = list(lyst)

    insrtCount = insertionSort(arr)
    print insrtCount

    print (insrtCount - swap_count)




if __name__ == "__main__":
    main()