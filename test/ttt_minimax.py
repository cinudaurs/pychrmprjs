
"""
Mini-max Tic-Tac-Toe Player
"""


import poc_ttt_provided as provided

# Set timeout, as mini-max can take a long time


# SCORING VALUES - DO NOT MODIFY
SCORES = {provided.PLAYERX: 1,
          provided.DRAW: 0,
          provided.PLAYERO: -1}

def mm_move(board, player):
    """
    Make a move on the board.

    Returns a tuple with two elements.  The first element is the score
    of the given board and the second element is the desired move as a
    tuple, (row, col).
    """

    empty_squares = board.get_empty_squares()

    if player is provided.PLAYERX:
        best_score = float("-inf")
    else:
        best_score = float("inf")

    who_won = board.check_win()

    if who_won != None:
        return SCORES[who_won], (-1,-1)

    else:

        best_move = None

        for square in empty_squares:
            board_clone = board.clone()
            board_clone.move(square[0],square[1],player)
            score = mm_move(board_clone, provided.switch_player(player))

            if player is provided.PLAYERX:

                if score[0] == 1:
                    best_score = score[0]
                    best_move = square
                    return best_score, best_move

                if score[0] > best_score:
                    best_score = score[0]
                    best_move = square

            else:

                if score[0] == -1:
                    best_score = score[0]
                    best_move = square
                    return best_score, best_move


                if score[0] < best_score:
                    best_score = score[0]
                    best_move = square


    return best_score, best_move


def move_wrapper(board, player, trials):
    """
    Wrapper to allow the use of the same infrastructure that was used
    for Monte Carlo Tic-Tac-Toe.
    """
    move = mm_move(board, player)
    assert move[1] != (-1, -1), "returned illegal move (-1, -1)"
    return move[1]

# Test game with the console or the GUI.
# Uncomment whichever you prefer.
# Both should be commented out when you submit for
# testing to save time.

provided.play_game(move_wrapper, 1, False)
# poc_ttt_gui.run_gui(3, provided.PLAYERO, move_wrapper, 1, False)


