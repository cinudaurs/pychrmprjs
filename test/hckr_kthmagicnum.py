from poc_queue import Queue

__author__ = 'srini'


def get_kth_magic_num(k):
    '''
    Design an algorithm to find the kth number such that the only prime factors are 3,
    5, and 7
    '''


    if k <= 0:
        return 0

    val = 1

    q3 = Queue()
    q5 = Queue()
    q7 = Queue()

    q3.enqueue(3)
    q5.enqueue(5)
    q7.enqueue(7)


    k-=1

    while k > 0:

        val = min(q3.peek(), q5.peek(), q7.peek())

        if val == q7.peek():
            q7.enqueue(val * 7)
            q7.dequeue()


        else:
            if val == q5.peek():

                q5.enqueue(val * 5)
                q7.enqueue(val * 7)
                q5.dequeue()

            else:  # must be from q3

                q3.enqueue(val * 3)
                q5.enqueue(val * 5)
                q7.enqueue(val * 7)
                q3.dequeue()





        k -= 1


    return val

print get_kth_magic_num(13)