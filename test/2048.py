"""
Clone of 2048 game.
"""

import random
import poc_2048_gui




# Directions, DO NOT MODIFY
UP = 1
DOWN = 2
LEFT = 3
RIGHT = 4

# Offsets for computing tile indices in each direction.
# DO NOT MODIFY this dictionary.
OFFSETS = {UP: (1, 0),
           DOWN: (-1, 0),
           LEFT: (0, 1),
           RIGHT: (0, -1)}

def merge(line):
    """
    Helper function that merges a single row or column in 2048
    """

    result = [0] * len(line)
    index = 0

    merged = False


    for line_index in range(len(line)):

        if line[line_index] != 0:

            if line[line_index] != result[index]:
                index = result.index(0)
                result[result.index(0)] = line[line_index]
                merged = False
            else:
                if not merged:
                    result[index] = line[line_index] * 2
                    merged = True
                else:
                    index = result.index(0)
                    result[result.index(0)] = line[line_index]
                    merged = False

    return result



class TwentyFortyEight:
    """
    Class to run the game logic.
    """

    def __init__(self, grid_height, grid_width):

        self.grid = []
        self.grid_height = grid_height
        self.grid_width = grid_width
        self.reset()
        self.initial_tiles = {
                                    UP : list(tuple([0, dummy_x]) for dummy_x in range(grid_width)),
                                    DOWN : list(tuple([grid_height - 1, dummy_x]) for dummy_x in range(grid_width)),
                                    LEFT : list(enumerate(0 for dummy_x in range(grid_height))),
                                    RIGHT : list(enumerate(grid_width - 1 for dummy_x in range(grid_height)))
                                    }

    def reset(self):
        """
        Reset the game so the grid is empty.
        """
        self.grid  = [[0 for dummy_col in range(self.grid_width)] for dummy_row in range(self.grid_height)]

    def __str__(self):
        """
        Return a string representation of the grid for debugging.
        """
        return str(self.grid)

    def get_grid_height(self):
        """
        Get the height of the board.
        """
        return self.grid_height


    def get_grid_width(self):
        """
        Get the width of the board.
        """
        return self.grid_width



    def move(self, direction):
        """
        Move all tiles in the given direction and add
        a new tile if any tiles moved.
        """

        tmp_vert_list = []
        move_list = []
        tile_changed = False

        for tile in self.initial_tiles[direction]:

            tmp_vert_list = []

            if direction == RIGHT or direction == LEFT:
                iter_count = self.grid_width
            elif direction == UP or direction == DOWN:
                iter_count = self.grid_height


            for col in range(iter_count):
                tmp_vert_list.append(self.get_tile(tile[0] + col * OFFSETS[direction][0],tile[1] + col * OFFSETS[direction][1]))

            move_list = merge(tmp_vert_list)

            if move_list <> tmp_vert_list:
                 tile_changed = True

            for col in range(iter_count):
                 self.set_tile(tile[0] + col * OFFSETS[direction][0],tile[1] + col * OFFSETS[direction][1], move_list[col])


        if tile_changed:
             self.new_tile()



    def new_tile(self):
        """
        Create a new tile in a randomly selected empty
        square.  The tile should be 2 90% of the time and
        4 10% of the time.
        """
        zero_list = []
        for col in range(self.grid_width):
            for row in range(self.grid_height):
                if self.grid[row][col] == 0:
                    zero_list.append([row,col])

        if len(zero_list) > 0:
              zero_list = random.choice(zero_list)
              self.set_tile(zero_list[0], zero_list[1], random.choice([2,2,2,2,2,2,2,2,4]))

    def set_tile(self, row, col, value):
        """
        Set the tile at position row, col to have the given value.
        """
        self.grid[row][col] = value


    def get_tile(self, row, col):
        """
        Return the value of the tile at position row, col.
        """
        return self.grid[row][col]


poc_2048_gui.run_gui(TwentyFortyEight(4, 4))
