import datetime
import os, sys
import shutil
import gzip
import re
from datetime import timedelta
from subprocess import check_call


DATE_FORMAT = '%Y%m%d'
SEARCH_STRING = "total"


def daterange(start, end):
    def convert(date):
        try:
            date = datetime.datetime.strptime(date, DATE_FORMAT)
            return date.date()
        except TypeError:
            return date

    def get_date(n):
        return datetime.datetime.strftime(convert(start) + timedelta(days=n), DATE_FORMAT)

    days = (convert(end) - convert(start)).days
    if days <= 0:
        raise ValueError('The start date must be before the end date.')
    for n in range(0, days + 1):
        yield get_date(n)


def open_files(filelist):
    for file in filelist:
        try:
            print 'currently processing ' + file
            if file.endswith('.gz'):
                f = gzip.open(file, 'rt')
            else:
                f = open(file, 'rt')
            yield f
            f.close()
        except:
            pass


def concat(files):
    for f in files:
        yield f


def search_sort_file(file, file_sorted):
    new_lines = []

    try:
        f = open(file, "r")
        f_sorted = open(file_sorted, "w")

        try:
            # read all the lines into a list.
            lines = f.readlines()
            for line in lines:
                line = str(line)
                if re.search(SEARCH_STRING, line):
                    new_lines.append(line)
            new_lines.sort()
            f.close()
            f_sorted.writelines(new_lines)

        finally:
            f_sorted.close()



    except IOError:
        pass


def sort_file(file, file_sorted):
    new_lines = []

    try:
        f = open(file, "r")
        f_sorted = open(file_sorted, "w")

        try:
            # read all the lines into a list.
            lines = f.readlines()
            lines.sort()
            f.close()
            f_sorted.writelines(lines)  # Write a sequence of strings to a file

        finally:
            f_sorted.close()



    except IOError:
        pass


if __name__ == '__main__':

    args = sys.argv[1:]

    if len(args) < 2:
        print 'Please run eg: python collateMonthsLogs.py 20150910 20150920'
        sys.exit()

    elif len(args) > 2:
        print 'Please run eg: python collateMonthsLogs.py 20150910 20150920'
        sys.exit()
    else:
        start = args[0]
        end = args[1]

    lion102_file_list = []
    lion103_file_list = []

    lion102_lines = []
    lion103_lines = []

    agg_file_list = []

    new_lines = []

    FILE_PATH_DEST = '/na101/home/erights/erw_apps_live/oup_eac/vinod-tests/lion102_lion103_' + start + '_' + end

    FILE_PATH_LION102 = '/na101/home/erights/erw_apps_live/oup_eac/lion102/apache-tomcat-6.0.32/'
    FILE_PATH_LION103 = '/na101/home/erights/erw_apps_live/oup_eac/lion103/apache-tomcat-6.0.32/'

    LION102_AGG_FILE = 'lion102_agg.txt'
    LION102_AGG_FILE_SORTED = 'lion102_agg_total_sorted.txt'
    LION103_AGG_FILE = 'lion103_agg.txt'
    LION103_AGG_FILE_SORTED = 'lion103_agg_total_sorted.txt'
    LION_FINAL = 'lion_final.txt'
    LION_FINAL_SORTED = 'lion_final_sorted.txt'
    LION_FINA_SORTED_GZ = 'lion_final_sorted.txt.gz'

    os.mkdir(FILE_PATH_DEST, 0755)

    for i in list(daterange(start, end)):
        lion102_file_list.append(FILE_PATH_LION102 + "ws-" + i + ".log.gz")

        lion103_file_list.append(FILE_PATH_LION103 + "ws-" + i + ".log.gz")


        # for lion102 agg total sorted file.
    os.chdir(FILE_PATH_DEST)

    files_lion102 = open_files(lion102_file_list)
    concat_file_lines_lion102 = concat(files_lion102)

    for line in concat_file_lines_lion102:
        with open(LION102_AGG_FILE, 'a') as lion102_file:
            lion102_file.writelines(line)

    search_sort_file(LION102_AGG_FILE, LION102_AGG_FILE_SORTED)

    # for lion103 sorted agg file.
    files_lion103 = open_files(lion103_file_list)
    concat_file_lines_lion103 = concat(files_lion103)

    for line in concat_file_lines_lion103:
        with open(LION103_AGG_FILE, 'a') as lion103_file:
            lion103_file.writelines(line)

    search_sort_file(LION103_AGG_FILE, LION103_AGG_FILE_SORTED)

    agg_file_list = [LION102_AGG_FILE_SORTED, LION103_AGG_FILE_SORTED]

    open_f = open_files(agg_file_list)
    concat_agg_sorted_file_lines = concat(open_f)

    for line in concat_agg_sorted_file_lines:
        with open(LION_FINAL, 'a') as lion_final:
            lion_final.writelines(line)

    sort_file(LION_FINAL, LION_FINAL_SORTED)

    check_call(['gzip', LION_FINAL_SORTED])

    lion102_file.close()
    lion103_file.close()
    lion_final.close() 

