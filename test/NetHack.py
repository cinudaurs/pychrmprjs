__author__ = 'srini'

class LinearChamber(object):


    def __init__(self, bombs, force):
        """
        initialize the position of the left sharpnel and right sharpnels based on position of the bomb
        in the input bomb string.
        """
        self.bombs = bombs
        self.force = force
        self.result = [bombs]
        self.time_level_state_of_chamber = []

        self.chmbr_length = len(self.bombs)

        if self.chmbr_length < 1 or self.chmbr_length > 50:
            print 'bombs should contain between 1 and 50 characters inclusive'
            exit

        if self.force < 1 or self.force > 10:
            print 'force should be between 1 and 10'
            exit

        self.allowed_chars = ['.', 'B']

        for letter in self.bombs:
            if letter not in self.allowed_chars:
                print "Each character in bombs should be either a '.' or a 'B'"
                exit

        self.left_sharpnel_locs = set([])
        self.right_sharpnel_locs = set([])

        for idx, letter in enumerate(bombs):
            if letter == 'B':
                self.left_sharpnel_locs.add(idx)
                self.right_sharpnel_locs.add(idx)


    def update_sharpnels_loc(self, left, right):


        updated_left = set([])
        for sharpnel in left:
            updated_left.add(sharpnel - self.force)


        updated_right = set([])
        for sharpnel in right:
            updated_right.add(sharpnel + self.force)

        return updated_left, updated_right




    def explode(self):

        chamber_zero = '.' * len(self.bombs)

        while self.time_level_state_of_chamber != chamber_zero:

            self.left_sharpnel_locs, self.right_sharpnel_locs = self.update_sharpnels_loc(self.left_sharpnel_locs, self.right_sharpnel_locs)

            self.time_level_state_of_chamber = []
            for idx in range(self.chmbr_length):
                if idx in self.left_sharpnel_locs:
                    self.time_level_state_of_chamber.append('X' if idx in self.right_sharpnel_locs else '<')
                else:
                    self.time_level_state_of_chamber.append('>' if idx in self.right_sharpnel_locs else '.')


            self.time_level_state_of_chamber = ''.join(self.time_level_state_of_chamber)
            self.result.append(self.time_level_state_of_chamber)

        return self.result










bombs = "..B.BB..B.B..B..."
chmbr = LinearChamber(bombs, 1)
result = chmbr.explode()

for i in result:
    print i


# for i in  chmbr.result:
#     print i

# print chmbr.left_sharpnel_locs
#print chmbr.sharpnel_left_locs

# for idx in range(chmbr.chmbr_length):
#     print idx


# size_of_chamber = len(bombs)
#
# print size_of_chamber
#
# all_left_chamber = '.' * size_of_chamber

