import sys

"""
Name : Srinivas Thangalapally
email : shriniva@gmail.com
phone no: 270 535 5676

"""


__author__ = 'Srinivas Thangalapally'

class myDB(object):
    """
    A Simple in memory DB that receives commands via standard input (stdin), and writes appropriate responses to standard output (stdout).
    valid_commands: ['SET', 'GET', 'UNSET', 'NUMEQUALTO', 'END', 'ROLLBACK', 'BEGIN', 'ROLLBACK', 'COMMIT']

    """

    def __init__(self):

        """
        Initializes the data structures.

            _db: the db, initialized to a empty dictionary.

            _block_stack: maintains the list of transactions.

        """

        self._db = {}
        self._block_stack = []



    def set(self, name, value):

        """
        Set the variable name to the value value.

        """
        if self._block_stack:

            most_recent_transaction = self._block_stack.pop()

            if name in self._db and name not in most_recent_transaction:
                most_recent_transaction[name] = self._db[name]

            self._block_stack.append(most_recent_transaction)

        if value != 'NULL':
            self._db[name] = value
        else:
            self._db[name] = 'NULL'



    def get(self, name):

        """
         Print out the value of the variable name, or NULL if that variable is not set.
        """
        if name in self._db:
            return self._db[name]
        else:
            return 'NULL'



    def unset(self, name):

        """
         Unset the variable name, making it just like that variable was never set.
        """
        if name in self._db:
            self.set(name, 'NULL')




    def numequalto(self, value):

        """
        Print out the number of variables that are currently set to value. If no variables equal that value, print 0.
        """

        total_num_equal_to = 0

        if self._db:
                for name, val in self._db.items():
                    if self._db[name] == value:
                        total_num_equal_to += 1

        return total_num_equal_to



    def begin(self):

        """
        Open a new transaction block. Transaction blocks can be nested; a BEGIN can be issued inside of an existing block.
        """
        self._block_stack.append({})




    def rollback(self):

        """
        Undo all of the commands issued in the most recent transaction block, and close the block. Print nothing if successful, or print NO TRANSACTION if no transaction is in progress.
        """

        if self._block_stack:

            most_recent_transaction = self._block_stack.pop()

            if not most_recent_transaction:

                # we're here meaning we're at the very first begin and if we rollback at this stage, _db should be empty.
                self._db = {}

            for name, val in most_recent_transaction.items():
                if val != 'NULL':
                    self._db[name] = val
                else:
                    self._db[name] = 'NULL'
        else:
            print 'NO TRANSACTION'

    def commit(self):

        """
        Close all open transaction blocks, permanently applying the changes made in them. Print nothing if successful, or print NO TRANSACTION if no transaction is in progress.
        """
        if not self._block_stack:
            print 'NO TRANSACTION'

        self._block_stack = []


    def end(self):

        """
        Exit the program.
        """
        exit()


if __name__ == "__main__":

    mempi = myDB()

    line = sys.stdin.readline().strip()

    while line:

        args = line.split(' ')

        crud_commands = ['GET', 'SET', 'UNSET', 'NUMEQUALTO', 'ROLLBACK', 'COMMIT']

        if args[0] in crud_commands:

            if args[0] == 'SET':
                mempi.set(args[1], args[2])

            elif args[0] == 'GET':
                print mempi.get(args[1])

            elif args[0] == 'UNSET':
                mempi.unset(args[1])

            elif args[0] == 'NUMEQUALTO':
                print mempi.numequalto(args[1])

            elif args[0] == 'ROLLBACK':
                mempi.rollback()

            elif args[0] == 'COMMIT':
                mempi.commit()

        elif args[0] == 'BEGIN':
            mempi.begin()

        elif args[0] == 'END':
            mempi.end()

        else:
            print "Invalid Command" + args[0]


        line = sys.stdin.readline().strip()