__author__ = 'srini'

def isBalanced(line):

    line_lngth = len(line)

    stck_lst = []

    pairs = [('(', ')'), ('{', '}'), ('[', ']')]
    push_list = ['(', '{', '[']
    pop_list = [')', '}', ']']

    i = 0
    j = line_lngth

    if line[0] in pop_list:
        return False

    while i < j:

        if line[i] in push_list:
            stck_lst.append(line[i])
        else:
            if len(stck_lst) > 0:
                last_brckt = stck_lst.pop()
                tuple_pair = (last_brckt, line[i])

                if tuple_pair in pairs:
                    i+=1
                    continue
                else:
                    return False


        i +=1

    if len(stck_lst) == 0:
        return True

    return False



m = int(raw_input())
for i in range(m):
    line = list(raw_input())
    #print line
    if (isBalanced(line)):
        print 'YES'
    else:
        print 'NO'