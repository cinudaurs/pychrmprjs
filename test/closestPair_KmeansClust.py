__author__ = 'srinivas'


"""
Template for Project 3
Student will implement four functions:

slow_closest_pairs(cluster_list)
fast_closest_pair(cluster_list) - implement fast_helper()
hierarchical_clustering(cluster_list, num_clusters)
kmeans_clustering(cluster_list, num_clusters, num_iterations)

where cluster_list is a list of clusters in the plane
"""

import math
import alg_cluster



def pair_distance(cluster_list, idx1, idx2):
    """
    Helper function to compute Euclidean distance between two clusters
    in cluster_list with indices idx1 and idx2

    Returns tuple (dist, idx1, idx2) with idx1 < idx2 where dist is distance between
    cluster_list[idx1] and cluster_list[idx2]
    """
    return (cluster_list[idx1].distance(cluster_list[idx2]), idx1, idx2)


def slow_pairs(cluster_list):
    """
   Takes a list of Cluster objects and returns the set of all closest
   pairs where each pair is represented by the tuple (dist, idx1, idx2)
   with idx1 < idx2 where dist is the distance between
   the closest pair cluster_list[idx1] and cluster_list[idx2].
   """
    closest_pairs = ()
    min_dist=float('inf')
    for idx in range(0, len(cluster_list)):
        for jdx in range(0, len(cluster_list)):
            if idx < jdx:
                current_value = pair_distance(cluster_list, idx, jdx)[0]
                if current_value < min_dist:
                    closest_pairs=(current_value, idx, jdx)
                    min_dist = current_value
    return closest_pairs





def slow_closest_pairs(cluster_list):
    """
    Compute the set of closest pairs of cluster in list of clusters
    using O(n^2) all pairs algorithm

    Returns the set of all tuples of the form (dist, idx1, idx2)
    where the cluster_list[idx1] and cluster_list[idx2] have minimum distance dist.

    """

    tmp = []
    (min_dist, pair_i, pair_j) = (float('inf'), None, None)

    for clst_idx, cluster in enumerate(cluster_list):
        for nxt_idx, cluster2 in enumerate(cluster_list):
            if clst_idx != nxt_idx:
                dist = cluster.distance(cluster2)

                if dist < min_dist:
                    (min_dist, pair_i, pair_j) = (dist, cluster, cluster2)

                    idx = cluster_list.index(cluster)
                    jdx = cluster_list.index(cluster2)

                    if idx < jdx:
                        (min_dist, pair_i, pair_j) = (dist, idx, jdx)
                    else:
                        (min_dist, pair_i, pair_j) = (dist, jdx, idx)

                    tmp = []

                    tmp.append((min_dist, pair_i, pair_j))



                elif dist == min_dist:

                    idx = cluster_list.index(cluster)
                    jdx = cluster_list.index(cluster2)

                    if idx < jdx:
                        tmp.append((dist, idx , jdx))
                    else:
                        tmp.append((dist, jdx, idx))



    result = set([tuple(elem) for elem in tmp])

    return result






def fast_closest_pair(cluster_list):
    """
    Compute a closest pair of clusters in cluster_list
    using O(n log(n)) divide and conquer algorithm

    Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
    cluster_list[idx1] and cluster_list[idx2]
    have the smallest distance dist of any pair of clusters
    """

    def fast_helper(cluster_list, horiz_order, vert_order):
        """
        Divide and conquer method for computing distance between closest pair of points
        Running time is O(n * log(n))

        horiz_order and vert_order are lists of indices for clusters
        ordered horizontally and vertically

        Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
        cluster_list[idx1] and cluster_list[idx2]
        have the smallest distance dist of any pair of clusters

        """

        # base case
        num_points = len(horiz_order)

        if num_points <= 3:

            slow_clusters_list=[]
            for idx in horiz_order:
                slow_clusters_list.append(cluster_list[idx])

            return slow_pairs(slow_clusters_list)

        # divide
        else:
            horiz_left = horiz_order[:num_points/2]
            horiz_right = horiz_order[num_points/2:]

            vert_left, vert_right = [], []
            x_divider = horiz_left[-1]

            #lines 9-10
            vert_left = [dummy_y for dummy_y in vert_order if dummy_y in horiz_left]
            vert_right = [dummy_y for dummy_y in vert_order if dummy_y in horiz_right]

            elementl = fast_helper(cluster_list, horiz_left, vert_left)

            elementr = fast_helper(cluster_list, horiz_right, vert_right)

            (dist_left, pair_ileft, pair_jleft) = elementl
            (dist_right, pair_iright, pair_jright) = elementr

            if dist_left < dist_right:
                (dist, pair_i, pair_j) = (dist_left, pair_ileft, pair_jleft)
            else:
                (dist, pair_i, pair_j) = (dist_right, pair_iright, pair_jright)

            close_y = [dummy_y for dummy_y in vert_order  if abs(dummy_y - x_divider) < dist]

            num_close_y = len(close_y)

            if num_close_y > 1:

                for iy_coord in range(num_close_y-1):
                    for jy_coord in range(iy_coord+1, min(iy_coord+8, num_close_y)):
                        if abs(close_y[iy_coord]-close_y[jy_coord]) < dist:
                            (dist, pair_i, pair_j) = (abs(close_y[iy_coord]-close_y[jy_coord]), close_y[iy_coord], close_y[jy_coord])

            return (dist, pair_i, pair_j)


    # compute list of indices for the clusters ordered in the horizontal direction
    hcoord_and_index = [(cluster_list[idx].horiz_center(), idx)
                        for idx in range(len(cluster_list))]
    hcoord_and_index.sort()
    horiz_order = [hcoord_and_index[idx][1] for idx in range(len(hcoord_and_index))]

    # compute list of indices for the clusters ordered in vertical direction
    vcoord_and_index = [(cluster_list[idx].vert_center(), idx)
                        for idx in range(len(cluster_list))]
    vcoord_and_index.sort()
    vert_order = [vcoord_and_index[idx][1] for idx in range(len(vcoord_and_index))]

    # compute answer recursively
    answer = fast_helper(cluster_list, horiz_order, vert_order)
    return (answer[0], min(answer[1:]), max(answer[1:]))




def hierarchical_clustering(cluster_list, num_clusters):
    """
    Compute a hierarchical clustering of a set of clusters
    Note: the function mutates cluster_list

    Input: List of clusters, number of clusters
    Output: List of clusters whose length is num_clusters
    """

    closest_pairs = ()


    while len(cluster_list) > num_clusters:

        closest_pairs = slow_pairs(cluster_list)

        #lines 5-6
        cluster_list[closest_pairs[1]].merge_clusters(cluster_list[closest_pairs[2]])

        cluster_list.pop(closest_pairs[2])



    return cluster_list




def euclidean_dist(u,v):
    dx = u[0] - v[0]
    dy = u[1] - v[1]
    return math.sqrt(dx*dx + dy*dy)


def kmeans_clustering(cluster_list, num_clusters, num_iterations):
    """
    Compute the k-means clustering of a set of clusters
    Note: the function mutates cluster_list

    Input: List of clusters, number of clusters, number of iterations
    Output: List of clusters whose length is num_clusters
    """

    # initialize k-means clusters to be initial clusters with largest populations

    totpop_and_index =  [(cluster_list[idx].total_population(), idx, cluster_list[idx])
                           for idx in range(len(cluster_list))]


    totpop_and_index.sort(reverse=True)

    k_centers = []
    k_clusters = []


    #line2
    for idx in range(num_clusters):
        k_clusters.append(totpop_and_index[idx][2])
        k_centers.append((totpop_and_index[idx][2].horiz_center(), totpop_and_index[idx][2].vert_center()))




    for i in range(num_iterations):
        #initialize k empty sets C1, C2, C3..Ck

         #In line 4, you should represent an empty cluster as a Cluster object whose set of counties is empty and whose total population is zero.
         #empty clusters

        k_sets = [alg_cluster.Cluster(set([]),0,0,0,0) for i in range(len(k_centers))]

        #lines 5-7
        for clst_idx in range(len(cluster_list)):
            smallest_distance = float('inf')


            for k_idx in range(num_clusters):

                distance = euclidean_dist((cluster_list[clst_idx].horiz_center(),cluster_list[clst_idx].vert_center()), k_centers[k_idx])

                if distance < smallest_distance:
                    smallest_distance = distance
                    kset_idx = k_idx



        #line 7 should be implemented using the merge_cluster method from the Cluster class.
            k_sets[kset_idx].merge_clusters(cluster_list[clst_idx])



        #lines 8-9
        for centr_idx in range(num_clusters):
            k_centers[centr_idx] = (k_sets[centr_idx].horiz_center(), k_sets[centr_idx].vert_center())


    return k_sets
