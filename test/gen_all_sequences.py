__author__ = 'Srinivas'


"""
gen_all_sequences() for dummies (like me)
"""
def pprint(sequences):
    """
    Just a pretty printer.
    """
    for a_seq in sequences:
        print a_seq

def gen_all_sequences(outcomes, length):
    """
    Iterative function that enumerates the set of all sequences of
    outcomes of given length
    """
    ans = [ [] ]
    for dummy_idx in range(length):
        #print 'ans:', ans
        temp = []
        for seq in ans:
            #print 'seq:', seq
            for item in outcomes:
                new_seq = list(seq)
                new_seq.append(item)
                temp.append(new_seq)
                #print 'temp ='
                #pprint(temp)
                #print '-------'
        ans = temp
        #print 'ans <- temp'
        #print '========='
    return ans

def run_example1():
    """
    Example of all sequences
    """
    outcomes = set([0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1])
    length = 8
    seq_outcomes = gen_all_sequences(outcomes, length)

    print "RESULT is", len(seq_outcomes), "sequences of", str(length), "outcomes:"
    pprint(seq_outcomes)
    print len(seq_outcomes)

run_example1()