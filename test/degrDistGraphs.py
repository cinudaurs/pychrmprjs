"""
In Degree Distribution Graphs
"""
__author__ = 'Srinivas'

import random
import matplotlib.pyplot as plt
"""
Provided code for Application portion of Module 1

Imports physics citation graph
"""

# general imports
import urllib2

# Set timeout for CodeSkulptor if necessary
#import codeskulptor
#codeskulptor.set_timeout(20)


###################################
# Code for loading citation graph


EX_GRAPH0 = {
    0:set([1,2]),
    1:set([]),
    2:set([])
    }

EX_GRAPH1 = {
    0: set([1,4,5]),
    1: set([2,6]),
    2: set([3]),
    3: set([0]),
    4: set([1]),
    5: set([2]),
    6: set([])
    }

EX_GRAPH2 = {
    0 : set([1,4,5]),
    1 : set([2,6]),
    2 : set([3,7]),
    3 : set([7]),
    4 : set([1]),
    5 : set([2]),
    6 : set([]),
    7 : set([3]),
    8 : set([1,2]),
    9 : set([0,3,4,5,6,7])
    }

HW_TEST_GRAPH1 = {
    0: set([1,4,6]),
    1: set([0,2,6]),
    2: set([1,3,6]),
    3: set([2,6]),
    4: set([0,5]),
    5: set([4]),
    6: set([0,1,2,3]),
    7: set([])
    }


HW_TEST_GRAPH4 = {
    0: set([1,4,5]),
    1: set([2,6]),
    2: set([3,7]),
    3: set([7,0]),
    4: set([1]),
    5: set([2]),
    6: set([]),
    7: set([4,3])
    }



CITATION_URL = "http://storage.googleapis.com/codeskulptor-alg/alg_phys-cite.txt"

def load_graph(graph_url):
    """
    Function that loads a graph given the URL
    for a text representation of the graph

    Returns a dictionary that models a graph
    """
    graph_file = urllib2.urlopen(graph_url)
    graph_text = graph_file.read()
    graph_lines = graph_text.split('\n')
    graph_lines = graph_lines[ : -1]

    print "Loaded graph with", len(graph_lines), "nodes"

    answer_graph = {}
    for line in graph_lines:
        neighbors = line.split(' ')
        node = int(neighbors[0])
        answer_graph[node] = set([])
        for neighbor in neighbors[1 : -1]:
            answer_graph[node].add(int(neighbor))

    return answer_graph









def make_complete_graph(num_nodes):

    """
    Takes the number of nodes num_nodes and returns a dictionary corresponding
    to a complete directed graph with the specified number of nodes.
    A complete graph contains all possible edges subject to the restriction
    that self-loops are not allowed. The nodes of the graph should be numbered 0 to num_nodes - 1
    when num_nodes is positive. Otherwise, the function returns a dictionary corresponding to the empty graph.
    """



    if num_nodes > 0:
        return dict([(i, set([(i+j+1)%num_nodes for j in range(num_nodes -1)])) for i in range(num_nodes)])
    else:
        return {}




def compute_in_degrees(digraph):

    """
    Takes a directed graph digraph (represented as a dictionary) and
    computes the unnormalized distribution of the in-degrees of the graph.
    The function should return a dictionary whose keys correspond to in-degrees of nodes in the graph.
    The value associated with each particular in-degree is the number of nodes with that in-degree.
    In-degrees with no corresponding nodes in the graph are not included in the dictionary.
    """

    in_degree_dict = {}
    for key in digraph.keys():
        in_degree = 0
        adj_list_values = digraph[key]
        for value in adj_list_values:
            if key in digraph[value] :
                in_degree += 1

        in_degree_dict[key] = in_degree

    return in_degree_dict





def in_degree_distribution(digraph):


    """
    Takes a directed graph digraph (represented as a dictionary) and computes the unnormalized distribution
    of the in-degrees of the graph. The function should return a dictionary whose keys correspond to in-degrees of nodes in the graph.
    The value associated with each particular in-degree is the number of nodes with that in-degree.
    In-degrees with no corresponding nodes in the graph are not included in the dictionary.
    """



    in_degree_dist_dict = {}
    in_degree_dict = compute_in_degrees(digraph)

    for value in in_degree_dict.values():
        if value in in_degree_dist_dict:
            in_degree_dist_dict[value] += 1
        else:
            in_degree_dist_dict[value]=1


    return in_degree_dist_dict


print compute_in_degrees(HW_TEST_GRAPH1)


#citation_grph = load_graph(CITATION_URL)

#unnormalized_dist =  in_degree_distribution(citation_grph)
unnormalized_dist =  in_degree_distribution(HW_TEST_GRAPH1)



sum = 0
for key in unnormalized_dist.keys():
    sum+=unnormalized_dist[key]


print sum

norm_dist = dict([(key, unnormalized_dist[key] / float(sum)) for key in unnormalized_dist.keys()])

print norm_dist

plt.plot(norm_dist.values(), unnormalized_dist.keys(),  "bo")
plt.xlabel("no. of vertices with this in-degree")
plt.ylabel("in-degree")

plt.title("in-degree Distribution of citation graphs")
plt.show()

