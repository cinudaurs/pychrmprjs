__author__ = 'Srinivas'


def countInversions(a):

    n = len(a)
    b = []
    c = []

    if n == 1:
        return 0
    else:
        b = a[0:n/2]
        c = a[n/2:n]
        il = countInversions(b)
        ir = countInversions(c)

        im = merge(b, c, a)
        return il + ir + im


def merge(b, c, a):

    count = 0
    i = 0
    j = 0
    k = 0

    p = len(b)
    q = len(c)

    while i < p and j < q:
        if b[i] <= c[j]:
            a[k] = b[i]
            i = i + 1

        else:
            a[k] = c[j]
            j = j + 1
            count = count + p - i

        k = k + 1

    if i == p:
        a[k:p+q] = c[j:q]
    else:
        a[k:p+q] = b[i:p]

    return count





print countInversions([3])
