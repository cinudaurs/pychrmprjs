__author__ = 'srini'

'''
take each cell value for each row in matrix then take each value from each row.
'''

matrix_1 = [ [1, 3, 4], [2, 6, 8], [7, 9, 5] ]

matrix_2 = [
    [[2, 3, 6], [5, 4, 7]],
    [[3, 9, 0], [8, 4, 2]]

]

flat_1 = [x for row in matrix_1 for x in row]

flat_2 = []
for sublist1 in matrix_2:
    for sublist2 in sublist1:
        flat_2.extend(sublist2)

flat_list_cmprhnsn = [x for sublist1 in matrix_2
                      for sublist2 in sublist1
                      for x in sublist2
                        ]


flat_3 = [x**2 for row in matrix_1 for x in row]

print flat_1
print flat_3
print flat_2

print flat_list_cmprhnsn