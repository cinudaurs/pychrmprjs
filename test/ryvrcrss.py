__author__ = 'srini'


from collections import OrderedDict



def can_cross(river, pos=0, speed=0):

    if pos > len(river) - 1:
        return True
    if river[pos] == '~':
        return False

    # For each rock, the set of speeds at which it is reachable
    riverdict = OrderedDict((rock_pos, set([])) \
                             for rock_pos in xrange(len(river)) \
                             if river[rock_pos] == '*')

    riverdict[pos].add(speed)

    for rock_pos in sorted(riverdict):

        possible_speeds = set([])

        for spd in riverdict[rock_pos]:
            possible_speeds.add(spd + 1)
            if spd > 0:
                possible_speeds.add(spd)
                if spd > 1:
                    possible_speeds.add(spd - 1)

        for spd in possible_speeds:
            new_pos = rock_pos + spd
            if new_pos >= len(river):
                return True
            if new_pos in riverdict:
                riverdict[new_pos].add(spd)
    return False



river = '**~*~~*~~~*~~~~*~~~~*~~~~*~~~*~~*~~~*~~~~'

print can_cross(river)







