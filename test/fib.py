__author__ = 'sthangalapally'

from rcviz import callgraph, viz

@viz
def fib(num):
    if num == 0:
        return 0
    elif num == 1:
        return 1
    else:
        return fib(num - 1) + fib(num - 2)


fib(4)

callgraph.render("fib.png")