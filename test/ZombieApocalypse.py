"""
Student portion of Zombie Apocalypse mini-project
"""

import random
import math
import poc_grid
import poc_queue
#import poc_zombie_gui

# global constants
EMPTY = 0
FULL = 1
FOUR_WAY = 0
EIGHT_WAY = 1
OBSTACLE = "obstacle"
HUMAN = "human"
ZOMBIE = "zombie"


class Zombie(poc_grid.Grid):
    """
    Class for simulating zombie pursuit of human on grid with
    obstacles
    """

    def __init__(self, grid_height, grid_width, obstacle_list = None,
                 zombie_list = None, human_list = None):
        """
        Create a simulation of given size with given obstacles,
        humans, and zombies
        """
        poc_grid.Grid.__init__(self, grid_height, grid_width)
        if obstacle_list != None:
            for cell in obstacle_list:
                self.set_full(cell[0], cell[1])
        if zombie_list != None:
            self._zombie_list = list(zombie_list)
        else:
            self._zombie_list = []
        if human_list != None:
            self._human_list = list(human_list)
        else:
            self._human_list = []

    def clear(self):
        """
        Set cells in obstacle grid to be empty
        Reset zombie and human lists to be empty
        """
        poc_grid.Grid.clear(self)
        self._human_list = []
        self._zombie_list = []

    def add_zombie(self, row, col):
        """
        Add zombie to the zombie list
        """
        self._zombie_list.append((row,col))

    def num_zombies(self):
        """
        Return number of zombies
        """
        return len(self._zombie_list)

    def zombies(self):
        """
        Generator that yields the zombies in the order they were
        added.
        """
        # replace with an actual generator
        for zombie in self._zombie_list:
            yield zombie

    def add_human(self, row, col):
        """
        Add human to the human list
        """
        self._human_list.append((row,col))

    def num_humans(self):
        """
        Return number of humans
        """
        return len(self._human_list)

    def humans(self):
        """
        Generator that yields the humans in the order they were added.
        """
        # replace with an actual generator
        for human in self._human_list:
            yield human

    def compute_distance_field(self, entity_type):
        """
        Function computes a 2D distance field
        Distance at member of entity_queue is zero
        Shortest paths avoid obstacles and use distance_type distances
        """


        height = self.get_grid_height()
        width = self.get_grid_width()

        visited = poc_grid.Grid(height,width)

        #visited = [[EMPTY for dummy_col in range(width)]
        #               for dummy_row in range(height)]

        distance_field  = [[self.get_grid_width() * self.get_grid_height() for dummy_col in range(self.get_grid_width())]
                    for dummy_row in range(self.get_grid_height())]

        boundary = poc_queue.Queue()

        if entity_type == ZOMBIE:
            for dummy in self._zombie_list:
                boundary.enqueue(dummy)
        elif entity_type == HUMAN:
            for dummy in self._human_list:
                boundary.enqueue(dummy)

        #For cells in the queue, initialize visited to be FULL and distance_field to be zero.

        for cell in boundary:
            visited.set_full(cell[0], cell[1])
            distance_field[cell[0]][cell[1]] = 0

        while len(boundary) > 0:

            cell = boundary.dequeue()
            neighbors = self.four_neighbors(cell[0], cell[1])

            #For each neighbor_cell in the inner loop,
            # check whether the cell is passable
            # and update the neighbor's distance
            # to be the minimum of its current distance
            # and distance_field[cell_index[0]][cell_index[1]] + 1.


            for neighbor in neighbors:

                if visited.is_empty(neighbor[0], neighbor[1]) and self.is_empty(neighbor[0], neighbor[1]):
                    visited.set_full(neighbor[0], neighbor[1])
                    distance_field[neighbor[0]][neighbor[1]] = min(distance_field[neighbor[0]][neighbor[1]], distance_field[cell[0]][cell[1]] + 1)
                    boundary.enqueue(neighbor)

        return distance_field


    def move_humans(self, zombie_distance):
        """
        Function that moves humans away from zombies, diagonal moves
        are allowed
        """
        distance_list = []
        new_human_list = []

        for human_cell in self._human_list:

            distance_list = []

            neighbors = self.eight_neighbors(human_cell[0], human_cell[1])
            human_cell_distance = zombie_distance[human_cell[0]][human_cell[1]]
            distance_list.append((human_cell, human_cell_distance))
            for neighbor in neighbors:
                if self.is_empty(neighbor[0],neighbor[1]):
                    neighbor_distance_from_zombie = zombie_distance[neighbor[0]][neighbor[1]]
                    distance_list.append((neighbor,neighbor_distance_from_zombie))

            neighbor_with_max_dist = max(distance_list,key=lambda item:item[1])[0]
            new_human_list.append(neighbor_with_max_dist)

        self._human_list = new_human_list



    def move_zombies(self, human_distance):
        """
        Function that moves zombies towards humans, no diagonal moves
        are allowed
        """
        distance_list = []
        new_zombie_list = []

        for zombie_cell in self._zombie_list:

            distance_list = []

            neighbors = self.four_neighbors(zombie_cell[0],zombie_cell[1])
            zombie_cell_distance = human_distance[zombie_cell[0]][zombie_cell[1]]
            distance_list.append((zombie_cell, zombie_cell_distance))
            for neighbor in neighbors:
                if self.is_empty(neighbor[0],neighbor[1]):
                    neighbor_distance_from_zombie = human_distance[neighbor[0]][neighbor[1]]
                    distance_list.append((neighbor,neighbor_distance_from_zombie))

            neighbor_with_min_dist = min(distance_list,key=lambda item:item[1])[0]
            new_zombie_list.append(neighbor_with_min_dist)

        self._zombie_list = new_zombie_list

# Start up gui for simulation - You will need to write some code above
# before this will work without errors

# poc_zombie_gui.run_gui(Zombie(30, 40))