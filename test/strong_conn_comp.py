__author__ = 'srini'

import sys
import threading

# globals
visited = {}
finish = {}
leader = {}

N = 875714
t = 0  # finish timing order number
s = 0  # current source vertex


def load_graph(filename):
    """
    Function that loads a graph given the URL
    for a text representation of the graph

    Returns a dictionary that models a graph
    """
    f = open(filename, 'r')

    G = {}
    RevG = {}

    for idx in range(1, N + 1):
        G[idx] = []
        RevG[idx] = []

    for line in f:
        v1 = int(line.split()[0])
        v2 = int(line.split()[1])

        G[v1].append(v2)
        RevG[v2].append(v1)

    f.close()

    return G, RevG


def init():
    for idx in range(1, N + 1):
        visited[idx] = 0
        finish[idx] = 0
        leader[idx] = 0


def dfs(G, i):
    global t
    global s

    global visited
    global finish
    global leader

    visited[i] = 1

    leader[i] = s

    for node in G[i]:
        if visited[node] == 0:
            dfs(G, node)

    t += 1

    finish[i] = t


def dfs_loop(G, i=None):
    global t
    global s
    global leader

    i = N  # current_label : to keep track of ordering

    while i > 0:

        if visited[i] == 0:
            s = i

            dfs(G, i)

        i -= 1


def scc():
    init()

    G, RevG = load_graph("/home/srini/pycharmProjects/test/SCC.txt")

    print "Loaded graph with", len(G), "nodes"

    dfs_loop(RevG)  # results in finish populated with ordering of nodes

    # build a new graph with ordering of nodes as in finish
    newG = {}

    for i in range(1, N + 1):
        temp = []
        for x in G[i]:
            temp.append(finish[x])

        newG[finish[i]] = temp

    init()

    dfs_loop(newG)

    # statistics
    lst = sorted(leader.values())
    stat = []
    pre = 0
    for i in range(0, N - 1):
        if lst[i] != lst[i + 1]:
            stat.append(i + 1 - pre)
            pre = i + 1
    stat.append(N - pre)
    l = sorted(stat)
    l.reverse()
    print(l[0:5])



if __name__ == '__main__':
    threading.stack_size(67108864)  # 64MB stack
    sys.setrecursionlimit(2 ** 20)  # approx 1 million recursions
    thread = threading.Thread(target=scc)  # instantiate thread object
    thread.start()  # run program at target
