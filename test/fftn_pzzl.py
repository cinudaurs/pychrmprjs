"""
Loyd's Fifteen puzzle - solver and visualizer
Note that solved configuration has the blank (zero) tile in upper left
Use the arrows key to swap this tile with its neighbors
"""

# import poc_fifteen_gui

class Puzzle:
    """
    Class representation for the Fifteen puzzle
    """

    def __init__(self, puzzle_height, puzzle_width, initial_grid=None):
        """
        Initialize puzzle with default height and width
        Returns a Puzzle object
        """
        self._height = puzzle_height
        self._width = puzzle_width
        self._grid = [[col + puzzle_width * row
                       for col in range(self._width)]
                      for row in range(self._height)]

        if initial_grid != None:
            for row in range(puzzle_height):
                for col in range(puzzle_width):
                    self._grid[row][col] = initial_grid[row][col]

    def __str__(self):
        """
        Generate string representaion for puzzle
        Returns a string
        """
        ans = ""
        for row in range(self._height):
            ans += str(self._grid[row])
            ans += "\n"
        return ans

    #####################################
    # GUI methods

    def get_height(self):
        """
        Getter for puzzle height
        Returns an integer
        """
        return self._height

    def get_width(self):
        """
        Getter for puzzle width
        Returns an integer
        """
        return self._width

    def get_number(self, row, col):
        """
        Getter for the number at tile position pos
        Returns an integer
        """
        return self._grid[row][col]

    def set_number(self, row, col, value):
        """
        Setter for the number at tile position pos
        """
        self._grid[row][col] = value

    def clone(self):
        """
        Make a copy of the puzzle to update during solving
        Returns a Puzzle object
        """
        new_puzzle = Puzzle(self._height, self._width, self._grid)
        return new_puzzle

    ########################################################
    # Core puzzle methods

    def current_position(self, solved_row, solved_col):
        """
        Locate the current position of the tile that will be at
        position (solved_row, solved_col) when the puzzle is solved
        Returns a tuple of two integers
        """
        solved_value = (solved_col + self._width * solved_row)

        for row in range(self._height):
            for col in range(self._width):
                if self._grid[row][col] == solved_value:
                    return (row, col)
        assert False, "Value " + str(solved_value) + " not found"

    def update_puzzle(self, move_string):
        """
        Updates the puzzle state based on the provided move string
        """
        zero_row, zero_col = self.current_position(0, 0)
        for direction in move_string:
            if direction == "l":
                assert zero_col > 0, "move off grid: " + direction
                self._grid[zero_row][zero_col] = self._grid[zero_row][zero_col - 1]
                self._grid[zero_row][zero_col - 1] = 0
                zero_col -= 1
            elif direction == "r":
                assert zero_col < self._width - 1, "move off grid: " + direction
                self._grid[zero_row][zero_col] = self._grid[zero_row][zero_col + 1]
                self._grid[zero_row][zero_col + 1] = 0
                zero_col += 1
            elif direction == "u":
                assert zero_row > 0, "move off grid: " + direction
                self._grid[zero_row][zero_col] = self._grid[zero_row - 1][zero_col]
                self._grid[zero_row - 1][zero_col] = 0
                zero_row -= 1
            elif direction == "d":
                assert zero_row < self._height - 1, "move off grid: " + direction
                self._grid[zero_row][zero_col] = self._grid[zero_row + 1][zero_col]
                self._grid[zero_row + 1][zero_col] = 0
                zero_row += 1
            else:
                assert False, "invalid direction: " + direction

    ##################################################################
    # Phase one methods

    def lower_row_invariant(self, target_row, target_col):
        """
        Check whether the puzzle satisfies the specified invariant
        at the given position in the bottom rows of the puzzle (target_row > 1)
        Returns a boolean


        Tile zero is positioned at (i,j).
        All tiles in rows i+1 or below are positioned at their solved location.
        All tiles in row i to the right of position (i,j) are positioned at their solved location.

        """
        # replace with your code

        zero_row, zero_col = self.current_position(0,0)

        if (zero_row != target_row and zero_col != target_col):
            return False

        elif (zero_row == target_row and zero_col == target_col):
            if(zero_row == self._height-1 and zero_col == self._width-1):
                return True

            else:
                row_idx = target_row
                while row_idx < self._height:
                    for col_idx in range(self._width):
                        if row_idx == target_row:
                            if col_idx > target_col:
                                solved_value = (row_idx * self._width) + col_idx
                                if self._grid[row_idx][col_idx] != solved_value:
                                    return False
                        else:
                            solved_value = (row_idx * self._width) + col_idx
                            if self._grid[row_idx][col_idx] != solved_value:
                                    return False


                    row_idx+=1

                return True


    def bfs(self, grid,target_tile, i, j, obstcl):
        path = []
        visited = {(i,j):True}
        zero_pos = (i, j)
        dist_ij = {(i,j): None}
        path_ij = {(i,j): None}
        q=[zero_pos]

        dist_ij[(i,j)] = 0

        while len(q) > 0:
            v = q.pop(0)
            neighbors = self.four_neighbors(v[0], v[1])
            for nei in neighbors:
                if nei not in visited and nei not in obstcl:
                    if not v in path:
                        path_ij[nei] = v
                        dist_ij[nei] = dist_ij[(i,j)]+1
                        visited[nei] = True
                        q.append(nei)

        path = []

        x = target_tile

        path = [target_tile] + path

        while dist_ij[x] != 0 :
            x = path_ij[x]
            path = [x] + path



        moves = ""
        path_copy = path
        while len(path_copy) - 1 > 0:

            tuple_idx = 0

            if path_copy[tuple_idx][1] == path_copy[tuple_idx+1][1]:

                if path_copy[tuple_idx][0]-path_copy[tuple_idx+1][0] > 0:
                    moves += "u"
                else:
                    moves += "d"

            elif path_copy[tuple_idx][0] == path_copy[tuple_idx+1][0]:

                if path_copy[tuple_idx][1] - path_copy[tuple_idx+1][1] > 0 :
                    moves += "l"
                else:
                    moves += "r"

            path_copy.pop(tuple_idx)

        return moves


    def four_neighbors(self, row, col):
            """
            Returns horiz/vert neighbors of cell (row, col)
            """
            ans = []
            if row > 0:
                ans.append((row - 1, col))
            if row < self._height - 1:
                ans.append((row + 1, col))
            if col > 0:
                ans.append((row, col - 1))
            if col < self._width - 1:
                ans.append((row, col + 1))
            return ans


    def solve_interior_tile(self, target_row, target_col):
        """
        Place correct tile at target position
        Updates puzzle and returns a move string
        """
        # replace with your code

        target_tile_curr_pos = self.current_position(target_row,target_col)
        zero_tile_curr_pos = self.current_position(0,0)

        obstcl = []

        for row in range(self._height):
            for col in range(self._width):
                if row >= zero_tile_curr_pos[0] and col > zero_tile_curr_pos[1]:
                    obstcl.append((row,col))

        all_moves = ""

        move_zero_tile_target_loc = self.bfs(self._grid, target_tile_curr_pos,zero_tile_curr_pos[0],zero_tile_curr_pos[1],obstcl)
        self.update_puzzle(move_zero_tile_target_loc)

        all_moves += move_zero_tile_target_loc

        current_target_tile_pos = self.current_position(target_row,target_col)

        if current_target_tile_pos == (target_row,target_col):
            return all_moves

        else:

            if current_target_tile_pos[0] == 0 and self.current_position(0,0) =















            while current_target_tile_pos != (target_row,target_col):

                if current_target_tile_pos[0] == target_row:

                    zero_pos = self.current_position(0,0)
                    move_left = zero_pos[1]

                    while move_left > 0:
                        all_moves += "l"
                        move = "l"
                        self.update_puzzle(move)
                        if self.current_position(target_row,target_col) == (target_row,target_col):
                            return all_moves

                        if move_left >= 1:
                            move_left = move_left - 1

                    if move_left == 0:

                        all_moves += "u"
                        move = "u"
                        self.update_puzzle(move)
                        move_right = move_left

                    while move_right < target_col:
                        all_moves += "r"
                        move = "r"
                        self.update_puzzle(move)
                        if move_right < target_col:
                            move_right += 1

                    if move_right == target_col:
                        all_moves += "d"
                        move = "d"
                        self.update_puzzle(move)



                elif current_target_tile_pos[1] == target_col:

                    zero_pos = self.current_position(0,0)
                    move_left = zero_pos[1]
                    move_down = zero_pos[0]

                    if move_left > 0:
                        all_moves += "l"
                        move = "l"
                        self.update_puzzle(move)
                        move_left = move_left - 1
                        if self.current_position(target_row,target_col) == (target_row,target_col):
                            return all_moves

                    if move_left == 0:
                        while move_down < target_row:
                            all_moves += "d"
                            move = "d"
                            self.update_puzzle(move)

                            if move_down == self.current_position(target_row, target_col)[0]:
                                all_moves += "r"
                                move = "r"
                                self.update_puzzle(move)
                                move_up =  move_down
                                break

                            if move_down < target_row:
                                move_down = move_down + 1

                    while move_up > 0:
                        all_moves += "u"
                        move = "u"
                        self.update_puzzle(move)

                        if self.current_position(target_row, target_col) == (target_row, target_col):

                            zero_tile_curr_pos = self.current_position(0,0)
                            obstcl.append((target_row, target_col))

                            if zero_tile_curr_pos != (target_row, target_col-1):
                                move_zero_tile_target_loc_minus_one =  self.bfs(self._grid, (target_row, target_col-1),zero_tile_curr_pos[0],zero_tile_curr_pos[1],obstcl)
                                all_moves += move_zero_tile_target_loc_minus_one

                            return all_moves

                        if move_up >= 1:
                            move_up = move_up - 1

                else:




                current_target_tile_pos = self.current_position(target_row, target_col)



        return all_moves





        #return ""

    def solve_col0_tile(self, target_row):
        """
        Solve tile in column zero on specified row (> 1)
        Updates puzzle and returns a move string
        """
        # replace with your code
        return ""

    #############################################################
    # Phase two methods

    def row0_invariant(self, target_col):
        """
        Check whether the puzzle satisfies the row zero invariant
        at the given column (col > 1)
        Returns a boolean
        """
        # replace with your code
        return False

    def row1_invariant(self, target_col):
        """
        Check whether the puzzle satisfies the row one invariant
        at the given column (col > 1)
        Returns a boolean
        """
        # replace with your code
        return False

    def solve_row0_tile(self, target_col):
        """
        Solve the tile in row zero at the specified column
        Updates puzzle and returns a move string
        """
        # replace with your code
        return ""

    def solve_row1_tile(self, target_col):
        """
        Solve the tile in row one at the specified column
        Updates puzzle and returns a move string
        """
        # replace with your code
        return ""

    ###########################################################
    # Phase 3 methods

    def solve_2x2(self):
        """
        Solve the upper left 2x2 part of the puzzle
        Updates the puzzle and returns a move string
        """
        # replace with your code
        return ""

    def solve_puzzle(self):
        """
        Generate a solution string for a puzzle
        Updates the puzzle and returns a move string
        """
        # replace with your code
        return ""

# Start interactive simulation

#poc_fifteen_gui.FifteenGUI(Puzzle(4, 4))



#1
#grid = [[4, 10,1, 3],
#        [5, 13, 2, 7],
#        [8, 12, 6, 11],
#        [14, 9, 0, 15]
#        ]

#2
grid = [[4, 13,1, 3],
        [5, 10, 2, 7],
        [8, 12, 6, 11],
        [9, 0, 14, 15]
        ]


#3
#grid = [[4, 7,1, 3],
#        [5, 6, 2, 0],
#        [8, 9, 10, 11],
#        [12, 13, 14, 15]
#        ]


obj = Puzzle(4, 4, grid)

moves = obj.solve_interior_tile(3,1)

print moves





