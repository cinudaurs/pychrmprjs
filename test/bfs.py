def bfs(grid, target_tile, i, j, obstcl):
    path = []
    visited = {(i,j):True}
    zero_pos = (i, j)
    dist_ij = {(i,j): None}
    path_ij = {(i,j): None}
    q=[zero_pos]

    dist_ij[(i,j)] = 0

    while len(q) > 0:
        v = q.pop(0)
        neighbors = four_neighbors(grid, v[0], v[1])
        for nei in neighbors:
         if nei not in visited and nei not in obstcl:
            if not v in path:
                path_ij[nei] = v
                dist_ij[nei] = dist_ij[(i,j)]+1
                visited[nei] = True
                q.append(nei)

    path = []

    x = target_tile

    path = [target_tile] + path

    while dist_ij[x] != 0 :
        x = path_ij[x]
        path = [x] + path

    print path

    moves = ""
    path_copy = path
    while len(path_copy) - 1 > 0:

        tuple_idx = 0

        if path_copy[tuple_idx][1] == path_copy[tuple_idx+1][1]:

            if path_copy[tuple_idx][0]-path_copy[tuple_idx+1][0] > 0:
                moves += "u"
            else:
                moves += "d"

        elif path_copy[tuple_idx][0] == path_copy[tuple_idx+1][0]:

            if path_copy[tuple_idx][1] - path_copy[tuple_idx+1][1] > 0 :
                moves += "l"
            else:
                moves += "r"

        path_copy.pop(tuple_idx)

    return moves

def four_neighbors(grid, row, col):
             """
             Returns horiz/vert neighbors of cell (row, col)
             """
             ans = []
             if row > 0:
                 ans.append((row - 1, col))
             if row < 3:
                 ans.append((row + 1, col))
             if col > 0:
                 ans.append((row, col - 1))
             if col < 3:
                 ans.append((row, col + 1))
             return ans


def current_position(grid, solved_row, solved_col):
        """
        Locate the current position of the tile that will be at
        position (solved_row, solved_col) when the puzzle is solved
        Returns a tuple of two integers
        """
        solved_value = (solved_col + 4 * solved_row)

        for row in range(4):
            for col in range(4):
                if grid[row][col] == solved_value:
                    return (row, col)
        assert False, "Value " + str(solved_value) + " not found"


def move_target_to_its_pos(grid, target_tile, i, j, obstcl):

    move_zero_tile_target_loc = bfs(grid,target_tile,i,j,obstcl)

    curr_target_tile_pos = current_position(grid,i,j)

    moves = move_zero_tile_target_loc

    if curr_target_tile_pos == (i,j):

        #if lower_row_invariant(current_position(0,0)) <== have to check for this.

        return moves
        #else move_zero_tile_left_target_loc( obstcl_list.add(curr_target_tile_pos)

    #same row

    else:

        #move_up = i


        while curr_target_tile_pos != (i,j):

            zero_pos = current_position(grid,0,0)
            move_left = zero_pos[1]

            if curr_target_tile_pos[0] == i:

                while move_left > 0:
                    moves += "l"
                    if move_left >= 1:
                        move_left = move_left - 1

                if move_left == 0:

                    moves += "u"
                    move_right = move_left

                while move_right < j:
                    moves += "r"
                    if move_right < j:
                        move_right += 1

                if move_right == j:

                    if current_position(grid,i,j) == (i,j):
                        return moves
                    else:
                        moves += "d"

            curr_target_tile_pos = current_position(grid,i,j)




    return moves





#grid = [[4, 13, 1, 3],
#        [5, 10, 2, 7],
#        [8, 12, 6, 11],
#        [9, 0, 14, 15]
#]

#grid = [[4, 10, 1, 3],
#        [5, 13, 2, 7],
#        [8, 12, 6, 11],
#        [14, 9, 0, 15]
#]


grid = [[4, 10,1, 3],
        [5, 7, 2, 9],
        [8, 12, 6, 11],
        [0, 13, 14, 15]
        ]

moves = move_target_to_its_pos(grid, (1,3), 3, 0, [(3,1), (3,2),(3,3)])
print moves


#pos = current_position(grid,3,2)
#print moves
#print pos















