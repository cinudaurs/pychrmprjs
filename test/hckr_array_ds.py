#!/bin/python

import sys
'''
Input(stdin):
4
1 4 3 2

Output(stdout):
2 3 4 1

'''

n = int(raw_input().strip())
arr = map(int,raw_input().strip().split(' '))

size = len(arr)

temp_str = ''

while size > 0:

    temp_str += str(arr[size-1])+' '
    size = size-1

print temp_str


