__author__ = 'srini'



def swap(lyst, i, j):

    temp = lyst[i]
    lyst[i] = lyst[j]
    lyst[j] = temp


def quicksort(lyst):

    quicksortHelper(lyst, 0, len(lyst)-1)




def quicksortHelper(lyst, left, right):

    if left < right:
        pivotLocation = partition(lyst, left, right)
        print ' '.join(map(str,lyst))
        quicksortHelper(lyst, left, pivotLocation-1)
        quicksortHelper(lyst, pivotLocation+1, right)



def partition(lyst, left, right):

    global swap_count

    # Always select the last element in the 'sub-array' as a pivot.
    pivot = lyst[right]

    # Set boundary point to first position
    boundary = left

    # Move items less than pivot to the left
    for index in range(left, right):
        if lyst[index] < pivot:
           swap(lyst, index, boundary)
           boundary += 1


    # Exchange the pivot item and the boundary item
    swap(lyst, right, boundary)

    return boundary


def main():
    lyst = []

    n = int(raw_input().strip())
    lyst = map(int,raw_input().strip().split(' '))
    quicksort(lyst)




if __name__ == "__main__":
    main()