__author__ = 'srini'


class Node(object):

   def __init__(self, data=None, next_node=None, prev_node = None):
       self.data = data
       self.next = next_node
       self.prev = prev_node

class DoubleLinkedList(object):

    def __init__(self):
        self.head = None


    def SortedInsert(self, head, data):

        newnode = Node(data)
        prevnode = head

        if head is None :    # empty list
            head = newnode
            tail = head
            return head


        elif data < head.data :   # insert before head
            newnode.next = head
            head.prev = newnode
            head = newnode
            return head

        else :      # insert in the middle or after tail
            probe = head

            while probe is not None and probe.data < data :
                prevnode = probe
                probe = probe.next


            if probe is None:
                if data > prevnode.data :  # insert after tail
                    newnode.prev = prevnode
                    prevnode.next = newnode
                    tail = newnode
            else:                           # insert in the middle
                newnode.next = probe
                newnode.prev = probe.prev
                probe.prev.next = newnode
                probe.prev = newnode

            return head


    def Reverse(self, head):

        current = head
        newHead = head
        temp = None

        while current:

            temp = current.prev
            current.prev = current.next
            current.next = temp
            newHead = current
            current = current.prev

        return newHead


    def print_list(self, head):

        while head is not None:

            print head.data
            head = head.next




dblylnkdlst = DoubleLinkedList()

head = dblylnkdlst.head

head = dblylnkdlst.SortedInsert(head, 4)
head = dblylnkdlst.SortedInsert(head, 2)
head = dblylnkdlst.SortedInsert(head, 6)
head = dblylnkdlst.SortedInsert(head, 5)
head = dblylnkdlst.SortedInsert(head, 10)
head = dblylnkdlst.SortedInsert(head, 9)


dblylnkdlst.print_list(head)

head = dblylnkdlst.Reverse(head)

print '\n'

dblylnkdlst.print_list(head)


