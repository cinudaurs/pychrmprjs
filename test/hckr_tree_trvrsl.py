from poc_queue import Queue

__author__ = 'srini'

"""
Node is defined as
self.left (the left child of the node)
self.right (the right child of the node)
self.data (the value of the node)
"""



class Node(object):

    def __init__(self, data=None):
       self.data = data
       self.left = None
       self.right = None

    def add(self, val):
        """
        adds a new node to the tree
        """
        if val <= self.data:
            if self.left:
                self.left.add(val)
            else:
                self.left = Node(val)
        else:
            if self.right:
                self.right.add(val)
            else:
                self.right = Node(val)


class BinaryTree(object):

    def __init__(self):
        """create a empty binary tree"""
        self.root = None

    def add(self, value):
        """ insert value into proper location in binary tree"""
        if self.root is None:
            self.root = Node(value)
        else:
            self.root.add(value)


    def preOrder(self,root):

        if root != None:
            print root.data,
            if root.left != None:
                self.preOrder(root.left)
            if root.right != None:
                self.preOrder(root.right)


    def postOrder(self,root):

        if root.left != None:
            self.postOrder(root.left)
        if root.right != None:
            self.postOrder(root.right)
        print root.data,


    def inOrder(self,root):

        if root.left != None:
            self.inOrder(root.left)

        print root.data,

        if root.right != None:
            self.inOrder(root.right)

    def height(self, root):
        if root is None:
            return 0
        else:
            return max(self.height(root.left), self.height(root.right)) + 1



    def top_view(self,root):

        if root != None:

            self.top_view_go_left(root.left, True)

            print root.data,

            self.top_view_go_left(root.right, False)


    def top_view_go_left(self,node, goLeft):

        if goLeft:
                if node.left != None:
                    self.top_view_go_left(node.left, goLeft)
                print node.data,

        else:
            print node.data,
            if node.right != None:
                self.top_view_go_left(node.right, goLeft)

    def level_order(self, root):
        lyst = []
        qyu = Queue()

        if root != None:
            qyu.enqueue(root)

        while not qyu.isEmpty():
            node = qyu.dequeue()
            lyst.append(node.data)
            if node.left is not None:
                qyu.enqueue(node.left)
            if node.right is not None:
                qyu.enqueue(node.right)

        for i in lyst:
            print str(i),


bintree = BinaryTree()

bintree.add(1)


bintree.root.left = Node(2)
bintree.root.right = Node(3)

bintree.root.left.left = Node(4)
bintree.root.left.right = Node(5)

bintree.root.left.right.right = Node(6)
bintree.root.left.right.right.right = Node(7)

# bintree.root.left.left = Node(1)
# bintree.root.left.right = Node(4)
#
# bintree.root.left.left.right = Node(9)

#bintree.top_view(bintree.root)

bintree.level_order(bintree.root)





