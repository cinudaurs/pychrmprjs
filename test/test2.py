__author__ = 'sthangalapally'


import poc_queue as queue


UGRAPH10 = {0:set([]), 1:set([2,3,4]), 2:set([1,3]), 3:set([1,2]), 4:set([1]), 5:set([6]),
         6:set([5]), 7:set([])}


def bfs_visited(ugraph, start_node):
    """
   returns a set of all nodes visited by the algorithm
   """
    visited=set()
    visited.add(start_node)
    que=queue.Queue()
    que.enqueue(start_node)

    #start the main loop
    while 0 < len(que):
        #dequeue one element
        current_node=que.dequeue()
        for node in ugraph[current_node]:
            if None != ugraph.get(node, None):
                if node not in visited:
                    visited.add(node)
                    que.enqueue(node)

    return visited

def cc_visited(ugraph):
    """
   Takes the undirected graph ugraph and returns a list of sets,
   where each set consists of all the nodes (and nothing else)
   in a connected component, and there is exactly one set in the
   list for each connected component in ugraph and nothing else
   """
    remaining_nodes=list(item for item in ugraph.keys())
    connected_components=[]

    while 0 < len(remaining_nodes):
        node = remaining_nodes.pop(0)
        visited_nodes = bfs_visited(ugraph, node)
        if visited_nodes not in connected_components:
            connected_components.append(visited_nodes)
    return connected_components

def largest_cc_size(ugraph):
    """
   Takes the undirected graph ugraph and returns the size (an integer)
   of the largest connected component in ugraph.
   """
    max_size=0
    components = cc_visited(ugraph)
    for item in components:
        if max_size < len(item):
            max_size = len(item)

    return max_size

def compute_resilience(ugraph, attack_order):
    """
   Takes the undirected graph ugraph, a list of nodes attack_order
   and iterates through the nodes in attack_order. For each node
   in the list, the function removes the given node and its edges
   from the graph and then computes the size of the largest
   connected component for the resulting graph.
   """
    return_list = []
    return_list.append(largest_cc_size(ugraph))
    temp_graph = {}
    for a_key in ugraph.keys():
        temp_graph[a_key]=ugraph[a_key]

    for attack_node in attack_order:
        del temp_graph[attack_node]
        return_list.append(largest_cc_size(temp_graph))
    return return_list



def test_bfs():
    'test bfs_visited'
    for node in UGRAPH10.keys():
        print("start at: %s, bfs-visted: %s" %(node, bfs_visited(UGRAPH10,node)))

def test_cc():
    'test cc_visited, largest_cc_size'
    print("cc-visited: %s" %cc_visited(UGRAPH10))
    print("largest-cc-size: %s" %largest_cc_size(UGRAPH10))

def test_resilience():
    'test compute_resilience'
    print("when attack node: %s, the compute_resilience is: %s" \
    %([1,4,5], compute_resilience(UGRAPH10, [1,4,5])))


if __name__ == '__main__':

    test_bfs()
    test_cc()
    test_resilience()
