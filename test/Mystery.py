__author__ = 'Srinivas'

def mystery(a, l, r):
    if l > r:
        return -1
    m = ( l + r ) / 2
    if a[m] == m:
        return m
    else:
        if a[m] < m:
            return mystery(a, m + 1, r)
        else:
            return mystery(a, l, m-1)



#print mystery([3,1,0,2,6,5,7],0,7)
print mystery([-2,0,1,3,7,12,15],0,6)