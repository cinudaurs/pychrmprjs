"""
project 2
"""
import poc_queue as queue

def bfs_visited(ugraph, source_node):
    """
    Takes the undirected graph ugraph and the node start_node and returns the set consisting of all nodes that are visited by a breadth-first search that starts at start_node.
    """

    que = queue.Queue()

    visited = set([source_node])

    que.enqueue(source_node)

    while len(que) > 0:
        node = que.dequeue()

        for neighbor in ugraph[node]:
            if ugraph.get(neighbor, None) != None:
                if neighbor not in visited:
                    visited.add(neighbor)
                    que.enqueue(neighbor)

    return visited


def cc_visited(ugraph):
    """
    Takes the undirected graph ugraph and returns a list of sets, where each set consists of all the nodes (and nothing else) in a connected component,
    and there is exactly one set in the list for each connected component in ugraph and nothing else.

    """

    conc_comp = []
    for node in ugraph.keys():
        if node not in conc_comp:
            set_entry = bfs_visited(ugraph,node)
            if set_entry not in conc_comp:
                conc_comp.append(set_entry)

    return conc_comp

def largest_cc_size(ugraph):

    """
    Takes the undirected graph ugraph and returns the size (an integer) of the largest connected component in ugraph.

    """

    largest_size = 0
    list_of_ccs = []

    list_of_ccs = cc_visited(ugraph)

    for node in list_of_ccs:
         size_of_cc = len(node)
         if size_of_cc > largest_size:
            largest_size = size_of_cc
    return largest_size


def compute_resilience(ugraph, attack_order):

    """
    compute_resilience(ugraph, attack_order) -
    Takes the undirected graph ugraph, a list of nodes attack_order and iterates through the nodes in attack_order.
    For each node in the list, the function removes the given node and its edges from the graph and
    then computes the size of the largest connected component for the resulting graph.
    The function should return a list whose k+1th entry is the size of the largest connected component in the graph after the removal of the first k nodes in attack_order.
    The first entry (indexed by zero) is the size of the largest connected component in the original graph.

    """

    list_of_kentries = []
    list_of_kentries.append(largest_cc_size(ugraph))

    ugraph_copy = dict(ugraph)

    for node in attack_order:
        del ugraph_copy[node]
        list_of_kentries.append(largest_cc_size(ugraph_copy))
    return list_of_kentries

