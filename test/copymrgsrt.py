__author__ = 'srini'


def copymergesort(A):
    """Merge sort A and return a new collection"""

    if len(A) < 2:
        return A

    mid = len(A)/2
    left = copymergesort(A[:mid])
    right = copymergesort(A[mid:])

    i = j = 0
    B = []

    while len(B) < len(A):

        if j >= len(right) or (i < mid and left[i] < right[j]):
            B.append(left[i])
            i += 1
        elif j < len(right):
            B.append(right[j])
            j += 1

    return B




A = [4, 9, 2, 18, 14, 32, 21]
print copymergesort(A)



