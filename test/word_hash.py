import itertools

__author__ = 'Srinivas'

__author__ = 'Srinivas'

"""
Student code for Word Wrangler game
"""

import urllib2

WORDFILE = "assets_scrabble_words3.txt"


# Functions to manipulate ordered word lists

def remove_duplicates(list1):
    """
    Eliminate duplicates in a sorted list.

    Returns a new sorted list with the same elements in list1, but
    with no duplicates.

    This function can be iterative.
    """

    tmp_list = []

    for idx in list1:
        if idx not in tmp_list:
            tmp_list.append(idx)

    return tmp_list

def intersect(list1, list2):
    """
    Compute the intersection of two sorted lists.

    Returns a new sorted list containing only elements that are in
    both list1 and list2.

    This function can be iterative.
    """
    list1_len = len(list1)
    list2_len = len(list2)

    list1_idx = 0
    list2_idx = 0

    result = []

    while list1_idx < list1_len and list2_idx < list2_len:
        if list1[list1_idx] == list2[list2_idx]:
            result.append(list1[list1_idx])
            list1_idx = list1_idx + 1
            list2_idx = list2_idx + 1

        elif list1[list1_idx] > list2[list2_idx]:
            list2_idx = list2_idx + 1

        elif list1[list1_idx] < list2[list2_idx]:
            list1_idx = list1_idx + 1

    return result


# Functions to perform merge sort

def merge(list1, list2):
    """
    Merge two sorted lists.

    Returns a new sorted list containing all of the elements that
    are in both list1 and list2.

    This function can be iterative.
    """
    new_list = []
    while list1 and list2:
        if list1[0] == list2[0]:
            new_list.append(list1.pop(0))
            list2.pop(0)
        elif list1[0] < list2[0]:
            new_list.append(list1.pop(0))
        else:
            new_list.append(list2.pop(0))

    if list1:
        new_list.extend(list1)
    if list2:
        new_list.extend(list2)

    return new_list


def merge_sort(list1):
    """
    Sort the elements of list1.

    Return a new sorted list with the same elements as list1.

    This function should be recursive.
    """
    if len(list1) <= 1:
        return list1

    middle = len(list1) / 2
    left = list1[:middle]
    right = list1[middle:]

    left = merge_sort(left)
    right = merge_sort(right)

    return list(merge(left,right))

# Function to generate all strings for the word wrangler game

def gen_all_strings(word):
    """
    Generate all strings that can be composed from the letters in word
    in any order.

    Returns a list of all strings that can be formed from the letters
    in word.

    This function should be recursive.
    """
    result = []

    if word == "":
         return [""]
    else:
         first = word[0]
         rest =  word[1:]
         rest_strings = list(gen_all_strings(rest))
         for rest in rest_strings:
             result += [rest[:x] + first + rest[x:] for x in range(len(rest)+1)]

    return result + rest_strings


def hash (s):
    h = 7
    letters = "acdegilmnoprstuw"
    for i in range(len(s)):
        h = (h * 37 + letters.index(s[i]))

    return h



def word_with_hash(word, lnth, hash_code):
    new_list = []
    perms = itertools.permutations(word, r=lnth)
    for perm in perms:
        perm = ''.join(perm)
        if hash(perm) == hash_code:
            print perm
            break


print word_with_hash('acdegilmnoprstuw', 8, 25180466553932)


