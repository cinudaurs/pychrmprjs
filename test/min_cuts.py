import copy
import random
import math

__author__ = 'Srinivas'


def load_graph(filename):
    """
    Function that loads a graph given the URL
    for a text representation of the graph

    Returns a dictionary that models a graph
    """
    graph_file = f = open(filename, 'r')
    graph_text = graph_file.read()
    graph_lines = graph_text.split('\n')
    graph_lines = graph_lines[ : -1]

    print "Loaded graph with", len(graph_lines), "nodes"


    result_graph = {}
    for line in graph_lines:
        neighbors = line.split('\t')
        node = int(neighbors[0])
        result_graph[node] = []
        for neighbor in neighbors[1 : -1]:
            result_graph[node].append(int(neighbor))

    return result_graph


def choose_random_edge(G):

    #v1 = G.keys()[random.randint(0, len(G)-1)]
    #v2 = G[v1][random.randint(0,len(G[v1])-1)]

    v1 = random.choice(G.keys())
    v2 = random.choice(G[v1])


    return v1, v2


def merge_karger(G, v1, v2):

    # Adding the edges from the absorbed node:
        for edge in G[v2]:
            if edge != v1:  # this stops us from making a self-loop
                G[v1].append(edge)

    # Deleting the references to the absorbed node and changing them to the source node:
        for edge1 in G[v2]:
            G[edge1].remove(v2)
            if edge1 != v1:  # this stops us from re-adding all the edges in start.
                G[edge1].append(v1)
    # Remove v2's list
        del G[v2]


def min_cut(G):
    while len(G) > 2:
        v1, v2 = choose_random_edge(G)
        merge_karger(G, v1, v2)
    return len(G[G.keys()[0]])

def calc_iter(G):
    n = len(G)
    return int(n**2 * math.log(n))


def iter_min_cut(G):

    min_cut_val = min_cut(copy.deepcopy(G))

    # To get (1/n) failure probability,
    # repeat the basic procedure n^2 * log(n) times
    #N = calc_iter(G)
    N = 200

    for i in range(0,N):
        ith_run = min_cut(copy.deepcopy(G))
        if ith_run < min_cut_val:
            min_cut_val = ith_run

    print "Min cut value", min_cut_val




result_graph = load_graph('kargerMinCut.txt')
print result_graph

iter_min_cut(result_graph)

