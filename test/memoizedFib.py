__author__ = 'sthangalapally'

from rcviz import callgraph, viz

@viz
def memoizedfib(num, memo_dict):
    if num in memo_dict:
        return memo_dict[num]
    else:
        sum1 = memoizedfib(num - 1, memo_dict)
        sum2 = memoizedfib(num - 2, memo_dict)
        memo_dict[num] = sum1 + sum2
        return sum1 + sum2


memoizedfib(5, {0 : 0, 1 : 1})
callgraph.render("memoizedfib.png")
