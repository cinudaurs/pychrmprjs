__author__ = 'srinivas'

"""
Template for Project 3
Student will implement four functions:

slow_closest_pairs(cluster_list)
fast_closest_pair(cluster_list) - implement fast_helper()
hierarchical_clustering(cluster_list, num_clusters)
kmeans_clustering(cluster_list, num_clusters, num_iterations)

where cluster_list is a list of clusters in the plane
"""

import math
import alg_cluster
import time


def pair_distance(cluster_list, idx1, idx2):
    """
   Helper function to compute Euclidean distance between two clusters
   in cluster_list with indices idx1 and idx2

   Returns tuple (dist, idx1, idx2) with idx1 < idx2 where dist is distance between
   cluster_list[idx1] and cluster_list[idx2]
   """
    return (cluster_list[idx1].distance(cluster_list[idx2]), min(idx1, idx2), max(idx1, idx2))


def slow_closest_pairs(cluster_list):
    """
   Takes a list of Cluster objects and returns the set of all closest
   pairs where each pair is represented by the tuple (dist, idx1, idx2)
   with idx1 < idx2 where dist is the distance between
   the closest pair cluster_list[idx1] and cluster_list[idx2].
   """
    set_of_pairs = []
    closest_value=float('inf')
    for idx_i in range(0, len(cluster_list)):
        for idx_j in range(0, len(cluster_list)):
            if idx_i < idx_j:
                current_value = pair_distance(cluster_list, idx_i, idx_j)[0]
                if closest_value == current_value:
                    set_of_pairs.append((current_value, idx_i, idx_j))
                elif closest_value > current_value:
                    set_of_pairs=[(current_value, idx_i, idx_j)]
                    closest_value = current_value
    return set(set_of_pairs)

def slow_pairs(cluster_list):
    """
   Takes a list of Cluster objects and returns the set of all closest
   pairs where each pair is represented by the tuple (dist, idx1, idx2)
   with idx1 < idx2 where dist is the distance between
   the closest pair cluster_list[idx1] and cluster_list[idx2].
   """
    set_of_pairs = ()
    closest_value=float('inf')
    for idx_i in range(0, len(cluster_list)):
        for idx_j in range(0, len(cluster_list)):
            if idx_i < idx_j:
                current_value = pair_distance(cluster_list, idx_i, idx_j)[0]
                if closest_value > current_value:
                    set_of_pairs=(current_value, idx_i, idx_j)
                    closest_value = current_value
    return set_of_pairs


def fast_closest_pair(cluster_list):
    """
   Compute a closest pair of clusters in cluster_list
   using O(n log(n)) divide and conquer algorithm

   Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
   cluster_list[idx1] and cluster_list[idx2]
   have the smallest distance dist of any pair of clusters
   """

    def fast_helper(cluster_list, horiz_order, vert_order):
        """
       Divide and conquer method for computing distance between closest pair of points
       Running time is O(n * log(n))

       horiz_order and vert_order are lists of indices for clusters
       ordered horizontally and vertically

       Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
       cluster_list[idx1] and cluster_list[idx2]
       have the smallest distance dist of any pair of clusters

       """

        numPoints = len(horiz_order)

        # base case
        if numPoints <= 3:

            slow_clusters_list=[]
            for idx in horiz_order:
                slow_clusters_list.append(cluster_list[idx])

            return slow_pairs(slow_clusters_list)

        # divide

        else:
            median = len(horiz_order) // 2
            middle_x = 0.5 * (cluster_list[horiz_order[median-1]].horiz_center() + cluster_list[horiz_order[median]].horiz_center())

            #separate list by horizontal coordinate
            horizontal_left=horiz_order[:median]
            horizontal_right=horiz_order[median:]

            #lines 9-10
            vertical_left = [dummy_y for dummy_y in vert_order if dummy_y in horizontal_left]
            vertical_right = [dummy_y for dummy_y in vert_order if dummy_y in horizontal_right]

            # conquer
            tuple_left = fast_helper(cluster_list, horizontal_left, vertical_left)
            tuple_right = fast_helper(cluster_list, horizontal_right, vertical_right)

            #define minimum
            if tuple_left[0] < tuple_right[0]:
                min_value = tuple_left
            else:
                min_value = tuple_right

            #line 14
            list_s=[]
            for y_coord in vert_order:
                if min_value[0] > abs(cluster_list[y_coord].horiz_center() - middle_x):
                    list_s.append(y_coord)

            for index_u in range(0, len(list_s)-1):
                for index_v in range(index_u+1, min(index_u+3, len(list_s))):
                    candidate_value = pair_distance(cluster_list, list_s[index_u], list_s[index_v])
                    if candidate_value[0] < min_value[0]:
                        min_value = candidate_value

        return (min_value)

    # compute list of indices for the clusters ordered in the horizontal direction
    hcoord_and_index = [(cluster_list[idx].horiz_center(), idx)
                        for idx in range(len(cluster_list))]
    hcoord_and_index.sort()
    horiz_order = [hcoord_and_index[idx][1] for idx in range(len(hcoord_and_index))]

    # compute list of indices for the clusters ordered in vertical direction
    vcoord_and_index = [(cluster_list[idx].vert_center(), idx)
                        for idx in range(len(cluster_list))]
    vcoord_and_index.sort()
    vert_order = [vcoord_and_index[idx][1] for idx in range(len(vcoord_and_index))]

    # compute answer recursively
    answer = fast_helper(cluster_list, horiz_order, vert_order)
    return (answer[0], min(answer[1:]), max(answer[1:]))



def hierarchical_clustering(cluster_list, num_clusters):
    """
   Compute a hierarchical clustering of a set of clusters
   Note: the function mutates cluster_list

   Input: List of clusters, number of clusters
   Output: List of clusters whose length is num_clusters
   """

    return []




def kmeans_clustering(cluster_list, num_clusters, num_iterations):
    """
   Compute the k-means clustering of a set of clusters

   Input: List of clusters, number of clusters, number of iterations
   Output: List of clusters whose length is num_clusters
   """

    # initialize k-means clusters to be initial clusters with largest populations

    return []



start_slow = time.time()
slow_res = slow_closest_pairs(cluster_list)
slow_time = time.time() - start_slow

print slow_time

start_fast = time.time()
fast_res = fast_closest_pair(cluster_list)
fast_time = time.time() - start_fast

print fast_time


print "+++++++++++++++++++++++++++++++"

print slow_res
print fast_res

print "ratio of slow to fast = ", slow_time / fast_time
#assert fast_res in slow_res