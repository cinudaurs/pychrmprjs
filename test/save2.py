
"""
Project 3
"""
#from Module3.Cluster import Cluster
import random

__author__ = 'tany'

import math
import alg_cluster


class Cluster:
    """
    Class for creating and merging clusters of counties
    """

    def __init__(self, fips_codes, horiz_pos, vert_pos, population, risk):
        """
        Create a cluster based the models a set of counties' data
        """
        self._fips_codes = fips_codes
        self._horiz_center = horiz_pos
        self._vert_center = vert_pos
        self._total_population = population
        self._averaged_risk = risk


    def __repr__(self):
        """
        String representation assuming the module is "alg_cluster".
        """
        rep = "alg_cluster.Cluster("
        rep += str(self._fips_codes) + ", "
        rep += str(self._horiz_center) + ", "
        rep += str(self._vert_center) + ", "
        rep += str(self._total_population) + ", "
        rep += str(self._averaged_risk) + ")"
        return rep


    def fips_codes(self):
        """
        Get the cluster's set of FIPS codes
        """
        return self._fips_codes

    def horiz_center(self):
        """
        Get the averged horizontal center of cluster
        """
        return self._horiz_center

    def vert_center(self):
        """
        Get the averaged vertical center of the cluster
        """
        return self._vert_center

    def total_population(self):
        """
        Get the total population for the cluster
        """
        return self._total_population

    def averaged_risk(self):
        """
        Get the averaged risk for the cluster
        """
        return self._averaged_risk


    def copy(self):
        """
        Return a copy of a cluster
        """
        copy_cluster = Cluster(set(self._fips_codes), self._horiz_center, self._vert_center,
                               self._total_population, self._averaged_risk)
        return copy_cluster


    def distance(self, other_cluster):
        """
        Compute the Euclidean distance between two clusters
        """
        vert_dist = self._vert_center - other_cluster.vert_center()
        horiz_dist = self._horiz_center - other_cluster.horiz_center()
        return math.sqrt(vert_dist ** 2 + horiz_dist ** 2)

    def merge_clusters(self, other_cluster):
        """
        Merge one cluster into another
        The merge uses the relatively populations of each
        cluster in computing a new center and risk

        Note that this method mutates self
        """
        if len(other_cluster.fips_codes()) == 0:
            return self
        else:
            self._fips_codes.update(set(other_cluster.fips_codes()))

            # compute weights for averaging
            self_weight = float(self._total_population)
            other_weight = float(other_cluster.total_population())
            self._total_population = self._total_population + other_cluster.total_population()
            self_weight /= self._total_population
            other_weight /= self._total_population

            # update center and risk using weights
            self._vert_center = self_weight * self._vert_center + other_weight * other_cluster.vert_center()
            self._horiz_center = self_weight * self._horiz_center + other_weight * other_cluster.horiz_center()
            self._averaged_risk = self_weight * self._averaged_risk + other_weight * other_cluster.averaged_risk()
            return self

    def cluster_error(self, data_table):
        """
        Input: data_table is the original table of cancer data used in creating the cluster.

        Output: The error as the sum of the square of the distance from each county
        in the cluster to the cluster center (weighted by its population)
        """
        # Build hash table to accelerate error computation
        fips_to_line = {}
        for line_idx in range(len(data_table)):
            line = data_table[line_idx]
            fips_to_line[line[0]] = line_idx

        # compute error as weighted squared distance from counties to cluster center
        total_error = 0
        counties = self.fips_codes()
        for county in counties:
            line = data_table[fips_to_line[county]]
            singleton_cluster = Cluster(set([line[0]]), line[1], line[2], line[3], line[4])
            singleton_distance = self.distance(singleton_cluster)
            total_error += (singleton_distance ** 2) * singleton_cluster.total_population()
        return total_error


def slow_closest_pairs(cluster_list):
    """

    Compute the set of closest pairs of cluster in list of clusters
    using O(n^2) all pairs algorithm

    Returns the set of all tuples of the form (dist, idx1, idx2)
    where the cluster_list[idx1] and cluster_list[idx2] have minimum distance dist.

    Takes a list of Cluster objects and returns the set of all closest pairs where each pair is represented by the tuple
    (dist, idx1, idx2) with idx1 < idx2 where dist is the distance between the closest pair cluster_list[idx1] and
    cluster_list[idx2]. This function should implement the brute-force closest pair method described in BFClosestPair
    from Homework 3 with the two differences: a set of all closest pairs is returned consisting of all pairs that share
    the same minimal distance and the returned indices are ordered.
    """

    all_closest_pairs_set = set([])
    (min_dist, pair_i, pair_j) =  (float("inf"), None, None)
    len_cluster_list = len(cluster_list)
    for idx1 in range(len_cluster_list):
        for idx2 in range(len_cluster_list):
            if idx1 != idx2:
                first_cluster = cluster_list[idx1]
                second_cluster = cluster_list[idx2]
                dist = first_cluster.distance(second_cluster)

                if dist < min_dist:
                    (min_dist, pair_i, pair_j) = (dist, first_cluster, second_cluster)

                    idx = cluster_list.index(first_cluster)
                    jdx = cluster_list.index(second_cluster)

                    if idx < jdx:
                        (min_dist, pair_i, pair_j) = (dist, idx, jdx)
                    else:
                        (min_dist, pair_i, pair_j) = (dist, jdx, idx)

                    tmp = []

                    tmp.append((min_dist, pair_i, pair_j))



                elif dist == min_dist:

                    idx = cluster_list.index(first_cluster)
                    jdx = cluster_list.index(second_cluster)

                    if idx < jdx:
                        tmp.append((dist, idx , jdx))
                    else:
                        tmp.append((dist, jdx, idx))

                #if temp_dist < min_dist:
                #


                #if idx1 < idx2:


                    #closest_pair_tuple = (temp_dist, idx1, idx2)
                    #if all_closest_pairs_set:
                    #   any_result_set_elem = random.sample(all_closest_pairs_set, 1)[0]
                    #   dist_any_elem = any_result_set_elem[0]
                    #   if temp_dist < dist_any_elem:
                    #       all_closest_pairs_set = set([closest_pair_tuple])
                    #   elif temp_dist == dist_any_elem:
                    #       all_closest_pairs_set.add(closest_pair_tuple)
                    #
                    #else:
                    #    all_closest_pairs_set.add(closest_pair_tuple)

    result = set([tuple(elem) for elem in tmp])
    return result

#def slow_closest_pair(cluster_list):
#    """
#    Function return tuple from set of closest cluster pairs
#    """
#
#    set_of_pairs =  list(slow_closest_pairs(cluster_list))
#
#    return set_of_pairs



def fast_closest_pair(cluster_list):
    """
    Compute a closest pair of clusters in cluster_list
    using O(n log(n)) divide and conquer algorithm

    Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
    cluster_list[idx1] and cluster_list[idx2]
    have the smallest distance dist of any pair of clusters
    """

    #def divide_fast_helper(len_half_points, size_horiz_order_list, horiz_order, vert_order):
    #    """Divide implementation of fast helper"""
    #    left_part_horiz_indices = horiz_order[0 : len_half_points]
    #
    #    right_part_horiz_indices = horiz_order[len_half_points : size_horiz_order_list]
    #
    #
    #    left_part_horiz_indices_set = set(left_part_horiz_indices)
    #    left_part_vert_indices = [vert_order_ind for vert_order_ind in vert_order if vert_order_ind in left_part_horiz_indices_set]
    #    right_part_vert_indices_set = set(right_part_horiz_indices)
    #    right_part_vert_indices = [right_order_ind for right_order_ind in vert_order if right_order_ind in right_part_vert_indices_set]
    #    left_tuple = fast_helper(cluster_list, left_part_horiz_indices, left_part_vert_indices) # use the original cluster_list
    #    right_tuple = fast_helper(cluster_list, right_part_horiz_indices, right_part_vert_indices) # use the original cluster_list
    #    (dist, ind_i, ind_j) = left_tuple if left_tuple[0] < right_tuple[0] else right_tuple # min is based on the first element of tuple
    #    return (dist, ind_i, ind_j)

    def fast_helper(cluster_list, horiz_order, vert_order):
        """
        Divide and conquer method for computing distance between closest pair of points
        Running time is O(n * log(n))

        horiz_order and vert_order are lists of indices for clusters
        ordered horizontally and vertically

        Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
        cluster_list[idx1] and cluster_list[idx2]
        have the smallest distance dist of any pair of clusters

        """

        size_horiz_order_list = len(horiz_order)
        # base case
        if size_horiz_order_list <= 3:
            i_ind = 0
            temp_point_array = []
            for idx in horiz_order:
                temp_point_array.append(cluster_list[idx])

            set_of_pairs =  list(slow_closest_pairs(temp_point_array))
            #(base_dist, base_ind_i, base_ind_j) = slow_closest_pair(temp_point_array)

            base_dist = set_of_pairs[0][0]

            base_ind_i =  horiz_order[set_of_pairs[0][1]]
            base_ind_j = horiz_order[set_of_pairs[0][2]]

            #print "base case " + str((base_dist, base_ind_i, base_ind_j))
            return (base_dist, base_ind_i, base_ind_j)
        else:
            # divide

            #len_half_points = int(math.ceil(float (size_horiz_order_list) / 2))  # the number of points on each half
            len_half_points = len(horiz_order) // 2

            mid = 1.0 / 2 * (cluster_list[len_half_points - 1].horiz_center() + cluster_list[len_half_points].horiz_center()) # the horizontal coordinate of the vertical dividing line

            #(dist, ind_i, ind_j) = divide_fast_helper(len_half_points, size_horiz_order_list, horiz_order, vert_order)

            left_part_horiz_indices = horiz_order[0 : len_half_points]
            right_part_horiz_indices = horiz_order[len_half_points : ]


            left_part_horiz_indices_set = set(left_part_horiz_indices)
            left_part_vert_indices = [vert_order_ind for vert_order_ind in vert_order if vert_order_ind in left_part_horiz_indices_set]

            right_part_vert_indices_set = set(right_part_horiz_indices)
            right_part_vert_indices = [right_order_ind for right_order_ind in vert_order if right_order_ind in right_part_vert_indices_set]

            left_tuple = fast_helper(cluster_list, left_part_horiz_indices, left_part_vert_indices) # use the original cluster_list
            right_tuple = fast_helper(cluster_list, right_part_horiz_indices, right_part_vert_indices) # use the original cluster_list

            (dist, ind_i, ind_j) = left_tuple if left_tuple[0] < right_tuple[0] else right_tuple # min is based on the first element of tuple

            return (dist, ind_i, ind_j)








            # conquer
            s_list = [vert_ind for vert_ind in vert_order if abs(cluster_list[vert_ind].horiz_center() - mid) < dist]

            size_s_list = len(s_list)
            for u_ind in range(size_s_list - 1):
                v_ind = u_ind + 1
                while v_ind < (u_ind + 3 if u_ind + 3 < size_s_list - 1 else size_s_list - 1):
                    dist_su_sv = cluster_list[s_list[u_ind]].distance(cluster_list[s_list[v_ind]])
                    (dist, ind_i, ind_j) = (dist, ind_i, ind_j) if dist < dist_su_sv else (dist_su_sv, s_list[u_ind], s_list[v_ind])
                    v_ind += 1

        return (dist, ind_i, ind_j)

    # compute list of indices for the clusters ordered in the horizontal direction
    hcoord_and_index = [(cluster_list[idx].horiz_center(), idx)
                        for idx in range(len(cluster_list))]
    hcoord_and_index.sort()
    horiz_order = [hcoord_and_index[idx][1] for idx in range(len(hcoord_and_index))]

    # compute list of indices for the clusters ordered in vertical direction
    vcoord_and_index = [(cluster_list[idx].vert_center(), idx)
                        for idx in range(len(cluster_list))]
    vcoord_and_index.sort()
    vert_order = [vcoord_and_index[idx][1] for idx in range(len(vcoord_and_index))]

    # compute answer recursively
    answer = fast_helper(cluster_list, horiz_order, vert_order)
    return (answer[0], min(answer[1:]), max(answer[1:]))


def hierarchical_clustering(cluster_list, num_clusters):
    """
    Compute a hierarchical clustering of a set of clusters
    Note: the function mutates cluster_list

    Input: List of clusters, number of clusters
    Output: List of clusters whose length is num_clusters
    """

    new_cluster_list = list()
    for cluster_item in cluster_list:
        new_cluster_list.append(cluster_item)

    while len(new_cluster_list) > num_clusters:
        (_, cluster_ind_i, cluster_ind_j) = fast_closest_pair(new_cluster_list)
        cluster_item_i = new_cluster_list[cluster_ind_i]
        cluster_item_j = new_cluster_list[cluster_ind_j]
        new_cluster_list.append(cluster_item_i.merge_clusters(cluster_item_j))
        new_cluster_list.remove(cluster_item_i)
        new_cluster_list.remove(cluster_item_j)

    return new_cluster_list



def kmeans_clustering(cluster_list, num_clusters, num_iterations):
    """
    Compute the k-means clustering of a set of clusters

    Input: List of clusters, number of clusters, number of iterations
    Output: List of clusters whose length is num_clusters
    """

    # initialize k-means clusters to be initial clusters with largest populations

    return []


CLUSTER_1 = Cluster(set([]), -94.7344256677, -50.9769550253, 1, 0)
CLUSTER_2 = Cluster(set([]), -54.6001975875, 74.3321879926, 1, 0)
CLUSTER_3 = Cluster(set([]), 29.9058546234, -84.3807976576, 1, 0)
CLUSTER_4 = Cluster(set([]), 42.614084055, 45.9472708725, 1, 0)
CLUSTER_5 = Cluster(set([]), 70.9434213978, 33.2672666304, 1, 0)
CLUSTER_6 = Cluster(set([]), 13.5830876374, 82.8130167938, 1, 0)
CLUSTER_7 = Cluster(set([]), 12.0342971947, -9.85870410747, 1, 0)
CLUSTER_8 = Cluster(set([]), 39.4365321637, 9.6818102667, 1, 0)
CLUSTER_9 = Cluster(set([]), 28.3158367432, 40.6812408906, 1, 0)
CLUSTER_10 = Cluster(set([]), 74.5271134332, 98.1880217742, 1, 0)
CLUSTER_11 = Cluster(set([]), 98.8262934106, 12.8292151029, 1, 0)
CLUSTER_12 = Cluster(set([]), -45.1347814796, 12.7991907391, 1, 0)
CLUSTER_13 = Cluster(set([]), 68.5159536511, 79.5672020609, 1, 0)
CLUSTER_14 = Cluster(set([]), 3.86436573488, -36.1111914404, 1, 0)
CLUSTER_15 = Cluster(set([]), 90.2011346207, 2.31000416369, 1, 0)
CLUSTER_16 = Cluster(set([]), 62.0872815001, -57.4828694192, 1, 0)
CLUSTER_17 = Cluster(set([]), 49.6012823686, -62.4575792505, 1, 0)
CLUSTER_18 = Cluster(set([]), -85.9067572973, -19.8483977307, 1, 0)
CLUSTER_19 = Cluster(set([]), -78.7377954393, -18.4922971627, 1, 0)
CLUSTER_20 = Cluster(set([]), 8.3303317534, -23.501641678, 1, 0)
CLUSTER_21 = Cluster(set([]), 54.6709103231, -39.9104438097, 1, 0)
CLUSTER_22 = Cluster(set([]), 19.324115764, 64.2700281959, 1, 0)
CLUSTER_23 = Cluster(set([]), 9.42660282897, -25.1401017887, 1, 0)
CLUSTER_24 = Cluster(set([]), 25.1410143368, -65.1231140669, 1, 0)
CLUSTER_25 = Cluster(set([]), 71.1781983843, -43.1698181418, 1, 0)
CLUSTER_26 = Cluster(set([]), 83.8795708696, 82.6689726779, 1, 0)


#
#
# alg_cluster.Cluster(set([]), -15.6770949141, 66.1058287915, 1, 0), alg_cluster.Cluster(set([]), 65.2839690505, 70.8672649718, 1, 0), alg_cluster.Cluster(set([]), 49.1322016698, 37.7900386702, 1, 0), alg_cluster.Cluster(set([]), 55.4982913341, -2.49881155334, 1, 0), alg_cluster.Cluster(set([]), -12.9390523698, -11.0616671446, 1, 0), alg_cluster.Cluster(set([]), -29.7551662191, 39.0504520394, 1, 0), alg_cluster.Cluster(set([]), 65.1206295057, -56.0921457271, 1, 0), alg_cluster.Cluster(set([]), 22.0477474211, 64.3298952947, 1, 0), alg_cluster.Cluster(set([]), -67.2077798431, 85.6809677489, 1, 0), alg_cluster.Cluster(set([]), 19.0727141072, -25.7454160694, 1, 0), alg_cluster.Cluster(set([]), -47.9226816602, 77.0797727364, 1, 0), alg_cluster.Cluster(set([]), 58.7407949233, 40.7413842428, 1, 0), alg_cluster.Cluster(set([]), 77.8958967211, 64.4991196408, 1, 0), alg_cluster.Cluster(set([]), -31.6520718336, 55.6667546815, 1, 0), alg_cluster.Cluster(set([]), 24.6483808951, -10.8105379663, 1, 0), alg_cluster.Cluster(set([]), 91.1667127146, 89.0836687564, 1, 0), alg_cluster.Cluster(set([]), -70.1676036641, -59.9461567588, 1, 0), alg_cluster.Cluster(set([]), 92.2086411141, -33.8172374906, 1, 0), alg_cluster.Cluster(set([]), -60.8928011948, -29.8718953092, 1, 0), alg_cluster.Cluster(set([]), -66.9212267432, -26.1989924563, 1, 0), alg_cluster.Cluster(set([]), -6.83392848477, 34.0622810916, 1, 0), alg_cluster.Cluster(set([]), -51.9951905142, 44.306614379, 1, 0), alg_cluster.Cluster(set([]), -65.1946774993, -71.7063077651, 1, 0), alg_cluster.Cluster(set([]), 57.1692504657, 2.7819976431, 1, 0), alg_cluster.Cluster(set([]), -55.1052706531, -68.724861997, 1, 0), alg_cluster.Cluster(set([]), 51.1688134613, 27.0529161312, 1, 0), alg_cluster.Cluster(set([]), 3.89807830323, 35.1744746495, 1, 0), alg_cluster.Cluster(set([]), -46.3541418097, -66.5114250637, 1, 0), alg_cluster.Cluster(set([]), 29.3904924442, 88.1271579403, 1, 0), alg_cluster.Cluster(set([]), -7.09235785439, 83.7668778725, 1, 0), alg_cluster.Cluster(set([]), -72.0745696166, 98.0764216468, 1, 0), alg_cluster.Cluster(set([]), -76.8236482614, -53.6238941753, 1, 0), alg_cluster.Cluster(set([]), -10.116092819, 16.7433652792, 1, 0), alg_cluster.Cluster(set([]), -15.3946797765, -97.2004412804, 1, 0), alg_cluster.Cluster(set([]), 79.2325907174, -77.2527232563, 1, 0),

CLUSTER_TEXT_LIST = list([CLUSTER_1, CLUSTER_2, CLUSTER_3, CLUSTER_4, CLUSTER_5, CLUSTER_6, CLUSTER_7, CLUSTER_8,
                          CLUSTER_9, CLUSTER_10, CLUSTER_11, CLUSTER_12, CLUSTER_13, CLUSTER_14, CLUSTER_16, CLUSTER_17,
                          CLUSTER_18, CLUSTER_19, CLUSTER_20, CLUSTER_21, CLUSTER_22, CLUSTER_23, CLUSTER_24, CLUSTER_25,
                          CLUSTER_26])

slow_res = slow_closest_pairs(CLUSTER_TEXT_LIST)
print "slow_closest_pair " + str(slow_res)
#print "hierarchical_clustering " + str(hierarchical_clustering(CLUSTER_TEXT_LIST, 3))
fast_res = fast_closest_pair(CLUSTER_TEXT_LIST)
print "fast_closest_pair " + str(fast_res)


